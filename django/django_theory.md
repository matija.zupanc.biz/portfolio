# [Django](https://trello.com/c/gdoEu0GV/226-1-what-is-django-and-what-are-its-key-features)

## 1. What is Django and what are its key features?

Django is a high-level Python web framework that follows the Model-View-Controller (MVC) architectural pattern. It provides a structured and efficient way to build web applications by promoting code reusability and adhering to the DRY (Don't Repeat Yourself) principle. Here are some key features of Django:

1. **Object-Relational Mapping (ORM)**: Django provides a powerful ORM that allows developers to interact with databases using Python code instead of writing raw SQL queries. It supports various database engines such as PostgreSQL, MySQL, SQLite, and Oracle.

2. **URL routing**: Django includes a flexible URL routing system that helps map URLs to appropriate views. This enables clean and understandable URL patterns and allows for easy navigation within the application.

3. **Template engine**: Django has a built-in template engine that simplifies the process of generating dynamic HTML content. It separates the presentation logic from the business logic and allows the reuse of templates across multiple pages.

4. **Forms handling**: Django provides a robust form handling mechanism that simplifies the process of validating and processing user input. It includes features like form rendering, field validation, and handling form data in a secure manner.

5. **Authentication and authorization**: Django offers built-in authentication and authorization functionalities to handle user authentication, permissions, and access control. It supports various authentication methods, including username/password, social media logins, and token-based authentication.

6. **Admin interface**: Django's admin interface automatically generates a customizable and user-friendly administrative interface for managing data in the application. It allows easy creation, deletion, and modification of database records without writing additional code.

7. **Security features**: Django incorporates numerous security features such as protection against common web vulnerabilities (e.g., cross-site scripting, cross-site request forgery), password hashing, secure session management, and input sanitization.

8. **Caching**: Django provides a caching framework that allows you to cache dynamic content and improve the performance of your application. It supports various caching backends, including in-memory caching, file system caching, and database caching.

9. **Internationalization and localization**: Django has built-in support for internationalization (i18n) and localization (l10n). It allows developers to create applications that can be easily translated into multiple languages and localized for different regions.

10. **Scalability and versatility**: Django is designed to handle high-traffic and scalable web applications. It provides tools and features like database connection pooling, session management, and caching that help improve performance and scalability.

These are just a few of the key features offered by Django. Its extensive ecosystem of packages and libraries further extends its capabilities and makes it a popular choice for web application development.

## 2. What is Django ORM?

Django ORM (Object-Relational Mapping) provides a high-level abstraction layer for interacting with databases. It allows developers to work with databases using Python objects and methods, instead of writing raw SQL queries. The Django ORM maps the structure and relationships defined in your models to the corresponding database tables and provides an intuitive API to perform database operations.

Key aspects of Django ORM:

1. **Model definition**: Django ORM starts with defining models as Python classes. Each model class represents a database table, and its attributes define the fields of that table. The ORM provides a wide range of field types to represent different data types such as strings, integers, dates, relationships, etc.

2. **Querying**: Django ORM offers a rich set of methods and query expressions to perform database queries. You can easily retrieve, filter, sort, and aggregate data using methods like `.filter()`, `.exclude()`, `.order_by()`, `.annotate()`, etc. This allows you to construct complex database queries in a concise and readable manner.

3. **Relationships**: Django ORM supports various types of relationships, including *one-to-one*, *one-to-many*, and *many-to-many*. You can define these relationships in your models using fields like `ForeignKey`, `OneToOneField`, and `ManyToManyField`. The ORM takes care of managing the relationships and provides convenient ways to navigate between related objects.

4. **Lazy loading**: Django ORM employs lazy loading, which means that related objects are not fetched from the database until they are explicitly accessed. This optimization helps improve performance by reducing unnecessary database queries.

5. **Aggregation and annotation**: Django ORM allows you to perform aggregations and annotations on querysets. You can use methods like `.aggregate()`, `.count()`, `.sum()`, and `.annotate()` to calculate aggregated values or add additional computed fields to the query results.

6. **Transactions**: Django ORM supports transactions, which allow you to group database operations into atomic units. Transactions ensure data consistency by allowing you to roll back changes in case of errors or exceptions.

7. **Database migrations**: Django ORM's migration system manages database schema changes and keeps the database structure in sync with the model definitions. It automatically generates migration files that describe the changes to be applied to the database schema, making it easy to evolve your database schema over time.

## 3. MVC vs Django MVT?

MVC (Model-View-Controller) and MVT (Model-View-Template) are two architectural patterns used in web development. While MVC is a widely used pattern in various frameworks and languages, Django, a Python web framework, uses a modified version called MVT. Here's a comparison of MVC and Django's MVT:

MVC (Model-View-Controller):

1. **Model**: The model represents the data structure and logic of the application. It encapsulates the data and provides methods to manipulate and retrieve it.

2. **View**: The view handles the presentation logic and user interface. It displays the data to the user and captures user input.

3. **Controller**: The controller receives user input and acts as an intermediary between the model and view. It processes user requests, updates the model, and updates the view accordingly.

Django's MVT (Model-View-Template):

1. **Model**: Similar to MVC, the model represents the data structure and logic. It defines the database schema and provides methods to interact with the data.

2. **View**: In Django, the view is responsible for handling the request, processing the data, and deciding which template to render. It acts as the bridge between the model and template.

3. **Template**: The template is responsible for defining the presentation and structure of the output. It contains HTML, along with placeholders for dynamic data that are rendered using the view.

Key differences:

1. In MVC, the controller is responsible for updating both the model and view, whereas in MVT, the view handles the responsibility of updating the template (view) based on the model.

2. In MVT, the template is responsible for rendering the final output and displaying the data, whereas in MVC, the view primarily focuses on data presentation and capturing user input.

3. MVT separates the responsibility of updating the view (template) from handling user input, which can be handled in the view itself. In MVC, the controller is responsible for handling user input and updating the view accordingly.

Django's MVT pattern is similar to MVC but with a slight modification where the view takes on the role of the controller as well. The view in Django handles the request, interacts with the model to retrieve data, and determines the appropriate template to render. This design promotes code reusability, separation of concerns, and a clear separation between data, presentation logic, and user interface.

## 4. Django Models

A model is the single, definitive source of information about your data. It contains the essential fields and behaviors of the data you’re storing. Generally, each model maps to a single database table.

* Each model is a Python class that subclasses django.db.models.Model.

* Each attribute of the model represents a database field.

* Relationships: Models can have relationships with other models, such as one-to-one, one-to-many, and many-to-many relationships.

* Model methods: You can define methods within your model class to perform custom operations or calculations related to the model's data. These methods can be called on model instances to manipulate or retrieve specific data.

* Model meta options: Django provides meta options that allow you to customize various aspects of the model, such as the ordering of query results, verbose names, database table names, and more.

* With all of this, Django gives you an automatically-generated database-access API.

They represent the data structure and define the logical structure of the database tables used by the application.

## 5. Templates

The Django template system is a built-in component of Django that allows you to generate dynamic HTML, XML, or any other text-based formats. It separates the presentation logic from the Python code by providing a syntax for embedding dynamic data and control structures in templates.

Here's how the Django template system works:

1. **Template files**: Templates in Django are plain text files with a .html extension (though they can contain other text-based formats as well). These files reside in the templates directory of your app or project.

2. **Template tags and filters**: Django templates utilize template tags and filters to perform various operations. Template tags provide control flow logic (e.g., loops, conditions), while filters allow you to manipulate and format data (e.g., converting dates, capitalizing strings). These tags and filters are enclosed in {% %} and {{ }} delimiters, respectively.

3. **Context**: Templates receive data from views through a context. A context is a dictionary-like object that contains the variables and their values. The view populates the context and passes it to the template rendering process.

4. **Rendering**: To render a template, Django combines the template file with the provided context. During rendering, template tags and filters are evaluated, and dynamic data is replaced with actual values. The resulting output is a rendered template with all the dynamic elements resolved.

5. **Template inheritance**: Django templates support inheritance, allowing you to define a base template with common elements and then extend or override specific blocks in child templates. This promotes code reuse and maintains a consistent layout across multiple pages.

6. **Template loaders**: Django uses template loaders to locate and load templates from different sources. By default, it searches for templates in the templates directory of each app. However, you can configure additional loaders to load templates from databases, remote locations, or other custom sources.

7. **Template caching**: To improve performance, Django provides template caching mechanisms. You can cache the rendered output of a template for a specified duration, reducing the need for re-rendering the template on subsequent requests.

## 6. Views

In Django, views are Python functions or classes that handle the processing and response generation for a particular URL or URL pattern. Views are responsible for fetching data, performing logic, and rendering templates or returning other types of responses.

While FBVs offer a more straightforward and flexible approach, CBVs provide additional structure, reusability, and built-in functionality for common tasks like CRUD operations.

* **Function-based Views**: Function-based views are the simplest way to define a view in Django. These views are defined as Python functions that accept an HttpRequest object as the first parameter and return an instance of HttpResponse or its subclasses.

* **Class-based Views**: Class-based views provide a more powerful and flexible way to define views. These views are defined as Python classes that inherit from Django's built-in View or its subclasses. Class-based views use class methods to handle different HTTP methods (GET, POST, etc.) and provide reusable mixins for common functionality.

**Context and Template Rendering**: Views can pass data to templates by using the render shortcut or by manually creating an instance of HttpResponse and rendering a template with a context dictionary. The context dictionary contains the data to be passed to the template.

## 7. Static files

Static files include assets such as CSS, JavaScript, images, and other files that are not dynamically generated.

Handling static files in Django:

1. **Project Directory Organization**:

    * project_name/
        * project_name/
            * settings.py
            * urls.py
        * app_name/
            * ...
        * assets/
            * css/
            * js/
            * images/
        * static/
        * ...

2. **Configuration in `settings.py`**:

    * `STATIC_URL = '/static/'`
    * `STATIC_ROOT = os.path.join(BASE_DIR, 'static')`
    * `STATICFILES_DIRS = [os.path.join(BASE_DIR, 'assets'),]`

3. **Loading Static Files in Templates**:

    * `{% load static %}`
    * `<link rel="stylesheet" href="{% static 'css/style.css' %}">`
    * `<script src="{% static 'js/script.js' %}"></script>`
    * `<img src="{% static 'images/logo.png' %}" alt="logo">`

4. **Serving Static Files during Development**:

    * `python manage.py runserver`

    * Django will automatically serve static files from the directories defined in `STATICFILES_DIRS`.

5. **Collecting Static Files for Deployment**:

    * `python manage.py collectstatic`

    * This will gather static files from the `STATICFILES_DIRS` and copy them to the `STATIC_ROOT` directory.

6. **Serving Static Files in Production**:

    * In a production environment, configure your web server (e.g., Nginx or Apache) to serve static files directly from the `STATIC_ROOT` directory.

## 8. Django admin and manage.py

`django-admin` is Django’s command-line utility for administrative tasks.

In addition, `manage.py` is automatically created in each Django project. It does the same thing as `django-admin` but also sets the `DJANGO_SETTINGS_MODULE` environment variable so that it points to your project’s `settings.py` file.

Generally, when working on a single Django project, it’s easier to use `manage.py` than `django-admin`. If you need to switch between multiple Django settings files, use `django-admin` with `DJANGO_SETTINGS_MODULE` or the `--settings` command line option.

With them you can perform tasks such as running the development server, running database migrations, creating new Django apps, managing static files, running tests, and more. It acts as a command-line interface to interact with your Django project.

## 9. Django Signals

Django signals provide a way to allow decoupled applications to get notified when certain actions or events occur within a Django project. Signals are a form of "publisher-subscriber" pattern, where senders emit signals and receivers handle those signals.

The concept of Django signals revolves around three main components:

1. **Signal**: A signal is a simple Python object representing a particular event or action that occurs within the Django framework. Django provides built-in signals for common events like model instance creation, model instance deletion, and request/response handling. Signals can also be custom-defined for specific application-level events.

2. **Sender**: The sender is the entity responsible for sending the signal. In Django, senders are typically Django model classes or other components within the framework that trigger events. When a sender triggers an event, it emits a signal.

3. **Receiver**: A receiver is a function or method that gets executed when a signal is sent. Receivers are typically functions defined by developers that handle the signal and perform specific actions or operations in response to the event. Receivers can be connected to signals using the `@receiver` decorator or the `connect()` method.

The workflow of Django signals can be summarized as follows:

1. **Define a signal**: You can use Django's built-in signals or define your own custom signals using the `django.dispatch.Signal` class.

2. **Define receiver functions**: Create functions that will handle the signals when they are sent. These receiver functions should accept specific arguments based on the signal being handled. Receiver functions are typically registered in the `signals.py` file of your application.

3. **Connect receivers to signals**: Connect the receiver functions to the signals they will handle. This can be done using the `@receiver` decorator or the `connect()` method provided by the `Signal` class.

4. **Send signals**: At the appropriate points in your code, trigger or send the signals using the `send()` method of the signal object. This can be done within Django models, views, or other components when specific events occur.

5. **Handle signals**: When a signal is sent, the connected receiver functions will be invoked automatically, allowing them to perform the desired actions or operations in response to the event.

The concept of Django signals promotes loose coupling and decoupling of components within a Django project. It enables different parts of the application to communicate and react to events without direct dependencies. Signals are commonly used for tasks such as logging, sending notifications, triggering updates, or performing additional processing based on specific events occurring within the project.

## 10. Caching

The process of storing and retrieving data in a temporary storage area, known as a cache, to improve the performance and response time of web applications. Caching helps to avoid repetitive and resource-intensive operations by storing the results of expensive computations, database queries, or external API calls in memory or disk-based storage.

Django provides built-in support for caching through its caching framework, which allows developers to easily integrate caching into their applications. The key components of the caching framework include cache backends, cache keys, and cache decorators.

1. **Cache Backends**: Django supports various cache backends, such as in-memory caching using the local memory cache or disk-based caching using file-based cache or database cache. Developers can choose the appropriate cache backend based on their application's needs.

2. **Cache Keys**: A cache key is a unique identifier associated with a specific piece of data that is stored in the cache. It is used to retrieve and store cached data. Cache keys are typically generated based on the parameters or properties of the data being cached, ensuring that each piece of data has a distinct cache key.

3. **Cache Decorators**: Django provides cache decorators that can be applied to views or functions to cache their results. The decorators automatically handle the process of caching and retrieving data based on the cache settings and cache keys. Decorators like `cache_page` and `cache_control` are commonly used to cache entire views or specific HTTP responses.

By utilizing caching in Django, repetitive or expensive operations can be avoided, resulting in faster response times, reduced load on the database or external services, and improved overall performance of the application.

It's important to note that caching should be used judiciously and with consideration for data consistency. Cache invalidation strategies, such as setting an appropriate cache timeout or manually invalidating the cache when data changes, should be implemented to ensure the cached data remains up to date.

## 11. Middleware

In Django, middleware is a component that sits between the web server and the view, allowing you to process requests and responses globally across your Django project. Middleware provides a way to modify or analyze HTTP requests and responses, perform authentication and authorization checks, handle exceptions, and add extra functionality to the request/response processing pipeline.

The purpose of middleware in Django is to enable you to perform common tasks and apply behavior consistently across multiple views.

Common use cases of middleware include:

1. **Request/response processing**: Middleware can intercept and process incoming requests before they reach the view. It can modify the request, add additional data to the request context, or perform other operations based on the request parameters. Similarly, middleware can process the response generated by the view and modify it before it is returned to the client.

2. **Authentication and authorization**: Middleware can handle user authentication and authorization checks. It can verify user sessions, check user permissions, and redirect unauthenticated users to the login page or deny access to certain views based on authorization rules.

3. **Exception handling**: Middleware can catch and handle exceptions that occur during request processing. It can perform custom error handling, log exceptions, or return custom error responses to the client.

4. **Cross-site request forgery (CSRF) protection**: Django's built-in middleware provides CSRF protection. It adds a CSRF token to forms and verifies it on form submissions to prevent cross-site request forgery attacks.

5. **Caching**: Middleware can implement caching strategies to improve performance by caching responses and serving them directly from the cache without hitting the view.

6. **Content manipulation**: Middleware can modify the content of requests or responses. For example, it can compress responses, add HTTP headers, rewrite URLs, or modify cookies.

You can configure the order of middleware execution by specifying their order in the `MIDDLEWARE` setting in your Django project's settings file. Middleware classes are executed in the order they appear in the list.

## 12. Context

Django's context processors are functions that add data to the context of every template rendered by Django. They allow you to include common data or functionality in the context without explicitly passing it from every view. Context processors are executed before rendering each template, ensuring that the added data is available in all templates across your project.

To create a context processor in Django, you define a Python function that takes a request object as an argument and returns a dictionary of data to be added to the context. The function is then registered as a context processor in the Django settings.

## 13. Querysets

Django Querysets are a powerful feature of Django's Object-Relational Mapping (ORM) that allows you to interact with your database using Python code. Querysets represent a collection of database objects (model instances) that match certain conditions or criteria. They provide a convenient and expressive way to query and manipulate data in your Django project.

1. **Retrieving Objects**: You can use Querysets to retrieve objects from the database based on certain conditions using methods like `filter()`, `exclude()`, `get()`, or chaining multiple methods together. These methods allow you to specify conditions based on fields, relationships, or complex queries.

2. **Chaining Queryset Methods**: Queryset methods can be chained together to create more complex queries. Each method modifies the Queryset and returns a new Queryset, allowing you to build up the desired query step by step.

3. **Lazy Evaluation**: Querysets in Django follow lazy evaluation, meaning the database query is not executed immediately when you define the Queryset. The actual database query is executed when you evaluate or iterate over the Queryset or perform an action that requires accessing the database.

4. **Queryset Methods and Aggregations**: Querysets provide a wide range of methods for filtering, ordering, aggregating, and manipulating data. These methods include `order_by()`, `values()`, `annotate()`, `count()`, `distinct()`, `aggregate()`, and many more, allowing you to perform complex queries and calculations on your data.

## 14. Mixins

Django mixins are a way to reuse and share functionality among multiple Django classes or views. A mixin is a class that provides additional methods or attributes that can be easily incorporated into other classes by using multiple inheritance.

## 15. `select_related` vs `prefetch_related`

`select_related` and `prefetch_related` are two query optimization methods in Django that help reduce the number of database queries and improve performance when working with related objects.

1. `select_related`:

    * **Purpose**: `select_related` is used to fetch related objects in a single database query, using SQL JOINs.

    * **How it works**: When you use `select_related` on a queryset, Django follows foreign key relationships and retrieves related objects in the same query, reducing the need for additional queries later.

    * **Use cases**: Use `select_related` when you know you will need the related objects and want to minimize database hits. It is effective for one-to-one and many-to-one relationships.

    * **Example**: `MyModel.objects.select_related('related_field')`

2. `prefetch_related`:

    * **Purpose**: `prefetch_related` is used to retrieve related objects in a separate batch of queries, reducing the number of queries overall.

    * **How it works**: When you use `prefetch_related` on a queryset, Django retrieves the primary objects in one query and then fetches the related objects in a separate query or set of queries. It uses efficient lookup strategies and caches the results.

    * **Use cases**: Use `prefetch_related` when you have many-to-many or many-to-one relationships and need to retrieve related objects efficiently. It is helpful when you want to minimize the number of queries even if it means fetching extra data.

    * **Example**: `MyModel.objects.prefetch_related('related_field')`

Here's a summary of the differences between select_related and prefetch_related:

1. `select_related` fetches related objects using SQL JOINs in a single query. It is suitable for reducing the number of queries when working with one-to-one and many-to-one relationships.

2. `prefetch_related` retrieves related objects in a separate batch of queries, reducing the overall number of queries. It is useful for optimizing many-to-many and many-to-one relationships.

In general, `select_related` is best suited when you know you will need the related objects, while `prefetch_related` is more appropriate when you have larger querysets and want to minimize the number of queries even if it means fetching additional data. It's important to consider the specific relationship and data access patterns in your application to determine which optimization technique to use.

## 16. Q objects

Django's Q objects allow you to build complex queries by combining conditions using logical operators. Q objects encapsulate conditions and can be combined using `&` for `AND`, `|` for `OR`, and `~` for `NOT` operations. They provide flexibility for creating dynamic queries and are particularly useful when dealing with user-defined filters or complex search functionality. By using Q objects, you can construct intricate queries based on runtime conditions, enhancing the control over queryset filtering.

## 17. CRUD

[CRUD](https://www.geeksforgeeks.org/django-crud-create-retrieve-update-delete-function-based-views/)

CRUD stands for Create, Read, Update, and Delete, which are the four fundamental operations involved in persistent storage and manipulation of data.

With Django's built-in ORM (Object Relational Mapper), you can create, retrieve, update, and delete data from a database using Python code rather than SQL queries. In other words, Django allows you to perform these operations on your data without having to write any SQL.

## 18. Django admin

Django admin is a built-in feature of Django that provides an administrative interface for managing and controlling the data and functionality of a Django project. It offers a web-based interface that allows authorized users to interact with the database and perform various administrative tasks without the need for manual database manipulation or writing custom views.

The concept of Django admin can be summarized as follows:

1. **Automatic admin interface**

2. **CRUD operations**

3. **Model customization**: Defining the displayed fields, search fields, filters, ordering, and more. Customization options are provided through the use of `ModelAdmin` classes.

4. **Permissions and authentication**

5. **Extensibility**: Django admin is highly extensible, allowing you to customize and enhance its functionality. You can add custom actions, define custom views, create custom forms, and override default templates to tailor the admin interface to your project's specific needs.

6. **Integration with related models**

## 19. What is the purpose of migrations in Django?

Migrations are Django’s way of propagating changes you make to your models (adding a field, deleting a model, etc.) into your database schema. They’re designed to be mostly automatic, but you’ll need to know when to make migrations, when to run them, and the common problems you might run into.

You should think of migrations as a version control system for your database schema. `makemigrations` is responsible for packaging up your model changes into individual migration files - analogous to commits - and `migrate` is responsible for applying those to your database.

The migration files for each app live in a “migrations” directory inside of that app, and are designed to be committed to, and distributed as part of, its codebase. You should be making them once on your development machine and then running the same migrations on your colleagues’ machines, your staging machines, and eventually your production machines.

**PostgreSQL** is the most capable of all the databases here in terms of schema support.

## 19. What are the different types of HTTP methods used in Django? (Connected to CRUD)

1. **GET**: The GET method is used to retrieve a resource from the server. In Django, it is typically used for rendering HTML pages, displaying information, and fetching data from the server.

2. **POST**: The POST method is used to send data to the server for processing. It is commonly used for submitting forms, creating new resources, or updating existing resources. In Django, POST requests are often handled by views that perform data validation, processing, and database operations.

3. **PUT**: The PUT method is used to update an existing resource on the server. It sends the complete representation of the resource to be updated. In Django, PUT requests are less commonly used compared to POST requests. To handle PUT requests, you may need to include additional middleware or use a third-party library like Django REST framework.

4. **PATCH**: The PATCH method is similar to PUT but is used to partially update an existing resource. It sends only the changes or updates to the server, rather than sending the complete representation of the resource. Like PUT, handling PATCH requests may require additional setup or libraries in Django.

5. **DELETE**: The DELETE method is used to delete a resource on the server. It requests the server to remove the specified resource. In Django, DELETE requests are typically handled by views that perform the deletion of records from the database.

6. **HEAD**: The HEAD method is similar to GET but only requests the headers of a resource without retrieving the actual content. It is often used to retrieve metadata or check if a resource exists without incurring the overhead of retrieving the entire content.

7. **OPTIONS**: The OPTIONS method is used to retrieve the allowed methods, headers, and other capabilities of a resource. It is useful for determining what operations are supported by a particular URL.

## 20. How do you handle authentication and authorization in Django?

In Django, handling authentication and authorization is made easier through its built-in authentication framework. Here's how you can handle authentication and authorization in Django:

1. **Authentication**:

    * **User Registration**: Create a registration form or use Django's built-in `UserCreationForm` to allow users to register. When a user registers, create a new `User` object and save it in the database.

    * **User Login**: Use Django's built-in `AuthenticationForm` to handle user login. Verify the user's credentials, authenticate the user, and store the user's session information.

    * **User Logout**: Provide a logout view or use Django's built-in `logout` view to clear the user's session and log them out.

2. **Authorization**:

    * **User Permissions**: Define permissions in your models by setting the `permissions` attribute. Associate these permissions with user roles or groups.

    * **User Groups**: Create user groups to group users with similar roles or permissions together. Assign permissions using Django's admin interface or programmatically.

    * **Restricting Access**: Use Django's built-in `@login_required` decorator to restrict access to specific views. This ensures that only authenticated users can access those views.

    * **Permission Checks**: Use Django's `user.has_perm()` method or the `@permission_required` decorator to check if a user has specific permissions before allowing them to perform certain actions.

    * **Object-level Permissions**: Implement object-level permissions by overriding the `has_perm()` method in your models. This allows you to define custom logic to check if a user has permission to perform actions on specific objects.

Django's authentication framework also provides additional features such as password reset, password change, and user profile management. It integrates well with the Django admin interface and offers flexible customization options to meet your specific authentication and authorization requirements.

while Django's built-in authentication framework covers common use cases, complex or specialized authentication and authorization requirements may require additional customization or the use of third-party packages.

## 21. What are the different types of relationships available in Django models?

1. **One-to-One** (1:1) Relationship:

    * One-to-One: Use `OneToOneField` to define a one-to-one relationship between two models. Each instance of one model is associated with exactly one instance of the other model.

2. **Many-to-One** (M:1) Relationship:

    * Foreign Key: Use `ForeignKey` to establish a many-to-one relationship. It implies that many instances of one model are associated with a single instance of another model.

        * Related Name: Use the `related_name` attribute in the foreign key field to specify a custom name for the reverse relation.

        * Related Query Name: Use the `related_query_name` attribute in the foreign key field to specify a custom name for the reverse relation when performing queries.

    * Reverse Relation: Django automatically creates reverse relations when you define a foreign key relationship. It allows you to navigate from the "many" side of a relationship to the "one" side.

3. **Many-to-Many** (M:M) Relationship:

    * Many-to-Many: Use `ManyToManyField` to represent a many-to-many relationship between two models. It signifies that each instance of one model can be associated with multiple instances of another model, and vice versa.

4. **Generic Relations**:

    * Use `GenericForeignKey` and `ContentType` to create a generic relationship that allows a model to be associated with any other model.

    * A Django generic relationship is a type of relationship that allows you to create a one-to-many relationship with any target model. Say we have a social media application in which we can like a post, comment, or page. So we create a model using a content type module and use it as a generic relation with the models' post, comment, or page. The generic relationship allows us to connect to a single model like with multiple models such as post, comment, or page.

5. **Self-Referential Relationships**:

    * Self-Referential: Use a foreign key or many-to-many field to establish a relationship between instances of the same model.

## 22. Django's session framework?

Django's session framework provides a way to store and retrieve data for individual visitors (users) across multiple requests and sessions. It enables the server to maintain stateful behavior in an otherwise stateless HTTP protocol.

The primary purpose of Django's session framework is to allow developers to store and access user-specific data throughout a user's interaction with a web application.

The session framework in Django is automatically enabled when you include the `django.contrib.sessions` app in your project's `INSTALLED_APPS` setting.

Key purposes of the session framework:

1. **User Authentication**: The session framework facilitates user authentication and allows you to keep track of whether a user is logged in or not. It stores the user's authentication details, such as the user ID, securely in the session.

2. **Temporary Storage**: Sessions provide a means to store temporary data for individual users. This can include data like form inputs, user preferences, shopping cart contents, or any other information that needs to persist across multiple requests for a specific user.

3. **Cross-request Communication**: Sessions enable communication between different views and requests for a specific user. Data stored in the session can be accessed and modified across multiple views, allowing for the seamless flow of information within the user's session.

4. **Security and Integrity**: Django's session framework takes care of the security aspects of session handling. It automatically manages session cookies, ensuring they are secure and tamper-proof. It also handles session expiration and renewal, protecting against session hijacking and session fixation attacks.

5. **Backend Flexibility**: Django allows you to choose different session backends, such as using cookies, database storage, or cache-based storage. This flexibility enables you to adapt the session framework to suit your application's specific needs, performance requirements, and scalability.

## 23. Request - response cycle in Django

Steps involved in the request-response cycle in Django:

1. The client sends a request to the Django server.

2. Django receives the request and passes it to middlewares for processing.

3. The middlewares can modify the request or add additional functionality to it before passing it on to the URL router.

4. The URL router matches the request to a view function that can handle the request.

5. The view function processes the request, which may involve retrieving data from a database or performing some other operation.

6. The view function returns an HTTP response to Django.

7. Django passes the HTTP response to middlewares for processing.

8. The middlewares can modify the response or add additional functionality to it before returning it to the client.
