from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMessage

from celery import shared_task


@shared_task
def add():
    return print("Hello")


@shared_task
def send_email(request, user, user_email):
    mail_subject = 'Activate your user account.'
    message = render_to_string('registr_login/register_email.html', {
        'user': user.email,
        'domain': get_current_site(request).domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': default_token_generator.make_token(user),
        'protocol': 'https' if request.is_secure() else 'http'
    })
    email = EmailMessage(mail_subject, message, to=[user_email])
    email.content_subtype = 'html'
    email.send()
