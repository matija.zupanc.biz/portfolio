from django.db.models.signals import post_save
from django.dispatch import receiver
from dashboard import models


@receiver(post_save, sender=models.Sprint)
def generate_name(sender, instance, created, **kwargs):
    if created and not instance.name:
        # Generate the name
        generated_name = 'Sprint' + ' ' + str(instance.id)
        instance.name = generated_name
        instance.save()


post_save.connect(generate_name, sender=models.Sprint)
