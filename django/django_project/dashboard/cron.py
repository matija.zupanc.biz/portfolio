from django.contrib.auth.models import User
from django.utils import timezone
from django.core.mail import EmailMessage
from django.template.loader import render_to_string

from dashboard import models


def cron_hello():
    print("Hello Cronjob")


def task_productivity(user):
    # Get the date 7 days ago from the current date
    last_week = timezone.now() - timezone.timedelta(days=7)

    # Query completed tasks for the user with status "FN" and start_date
    # within the last 7 days
    done_tasks_last_week = models.Task.objects.filter(
        assigned_to=user, status="FN", last_status_change__gte=last_week)
    # Query total tasks assigned to user
    total_tasks = models.Task.objects.filter(assigned_to=user)

    if total_tasks.exists():
        # Calculate productivity for the last week
        productivity_last_week = round(
            (done_tasks_last_week.count() / total_tasks.count()) * 100, 2
        )
    else:
        productivity_last_week = 0

    return productivity_last_week


def send_productivity_to_user():
    users = User.objects.all()

    for user in users:
        productivity_last_week = task_productivity(user)

        mail_subject = 'Your Productivity Last Week'
        message = render_to_string('productivity_report.html', {
            'user': user.username,
            'productivity_last_week': productivity_last_week,
        })

        # Send the email to the user's email address
        email = EmailMessage(mail_subject, message, to=[user.email])
        email.content_subtype = 'html'
        email.send()

        print(f"{user.username} - {productivity_last_week}")
