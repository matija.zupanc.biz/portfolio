import json

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.contrib import messages
from django.utils.http import urlsafe_base64_decode
from django.utils.encoding import force_str
from django.db.models import Q
from django.contrib.auth.models import User

from dashboard import forms, models, tasks
from dashboard.utils import time_until_due


def register_user(request):
    if request.user.is_authenticated:
        return redirect('user_dashboard', pk=request.user.id)
    else:
        if request.method == 'POST':
            form = forms.CustomUserCreationForm(data=request.POST)
            if form.is_valid():
                # 'commit=False' means user is created in memory and not yet
                # in database
                user = form.save(commit=False)
                user.is_active = False
                form.save()

                # Send the email with the confirmation link
                tasks.send_email(request, user, form.cleaned_data.get('email'))

                request.session['current_user'] = {'email': user.email}
                return redirect('confirm_registr')
            else:
                context = {
                    'form': form,
                    'page_title': 'Registration',
                    'footer_register': 'registr_login/registration_footer.html'
                }
                return render(
                    request, 'registr_login/registration.html', context)

        form = forms.CustomUserCreationForm
        context = {
            'form': form,
            'page_title': 'Registration',
            'footer_register': 'registr_login/registration_footer.html'
        }
        return render(request, 'registr_login/registration.html', context)


def activate(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        return redirect('login_user')
    else:
        messages.error(request, 'Invalid activation link.')
        # TODO Failed Activation page
        # return redirect('activation_failed')


def confirm_registr(request):
    context = {
        'page_title': 'Confirm Registration',
        'footer_login': 'registr_login/login_footer.html'
    }
    return render(request, 'registr_login/confirm_registration.html', context)


def login_user(request):
    if request.user.is_authenticated:
        return redirect('user_dashboard', pk=request.user.id)
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('user_dashboard', pk=user.id)
            else:
                messages.info(request, 'Username or password is incorrect')

        context = {
            'page_title': 'Login',
            'footer_register': 'registr_login/login_footer.html'
        }
        return render(request, 'registr_login/login.html', context)


def logout_user(request):
    logout(request)
    return redirect('login_user')


@login_required(login_url='login_user')
def update_user(request, pk):
    user = models.User.objects.get(id=request.user.pk)

    if request.method == 'POST':
        user_form = forms.CustomUserChangeForm(request.POST, instance=user)

        if user_form.is_valid():
            user_form.save()
            context = {
                'page_title': 'Update Info',
                'user_form': user_form
            }
            return render(request, 'update_user.html', context)

    user_form = forms.CustomUserChangeForm(instance=user)
    context = {
        'page_title': 'Update Info',
        'user_form': user_form
    }
    return render(request, 'update_user.html', context)


@login_required(login_url='login_user')
def user_dashboard(request, pk='4'):
    current_user = request.user
    tasks = models.Task.objects.filter(assigned_to=current_user)
    if tasks:
        productivity = tasks[0].productivity
    else:
        productivity = 0

    # Compare the URL parameter with the currently logged-in user's identifier
    if current_user.pk != int(pk):
        pk = current_user.pk
        return redirect('user_dashboard', pk=pk)

    # Retrieve all projects associated with the user
    owned_projects_count = models.Project.objects.filter(
        owner=current_user).count()
    collab_projects_count = models.Project.objects.filter(
        collaborators=current_user).exclude(owner=current_user).count()

    # Retrieve all tasks associated with the user
    tasks_count = models.Task.objects.filter(assigned_to=current_user).count()

    # Queryset to retrieve all other collaborators
    user = User.objects.get(id=current_user.id)

    collaborators_count = User.objects.filter(
        Q(collaborating_projects__in=user.owned_projects.all()) |
        Q(owned_projects__collaborators=user)
    ).exclude(id=user.id).distinct().count()

    work_count = {
        'owned_projects_count': owned_projects_count,
        'collab_projects_count': collab_projects_count,
        'tasks_count': tasks_count,
        'collaborators_count': collaborators_count,
    }

    # Retrieve statuses - Projects
    done_project_count = models.Project.objects.filter(
        Q(owner=current_user) | Q(collaborators=current_user), status="CM"
    ).distinct().count()
    in_p_project_count = models.Project.objects.filter(
        Q(owner=current_user) | Q(collaborators=current_user), status="IP"
    ).distinct().count()
    behind_project_count = models.Project.objects.filter(
        Q(owner=current_user) | Q(collaborators=current_user), status="BH"
    ).distinct().count()

    total_projects = models.Project.objects.filter(Q(owner=current_user) | Q(
        collaborators=current_user)).distinct().count()
    if total_projects == 0 or done_project_count == 0:
        done_project_percent = 0
    else:
        done_project_percent = (done_project_count / total_projects) * 100

    if total_projects == 0 or in_p_project_count == 0:
        in_p_project_percent = 0
    else:
        in_p_project_percent = (in_p_project_count / total_projects) * 100

    if total_projects == 0 or behind_project_count == 0:
        behind_project_percent = 0
    else:
        behind_project_percent = (behind_project_count / total_projects) * 100

    # Retrieve statuses - Tasks
    done_task_count = models.Task.objects.filter(
        assigned_to=current_user, status="FN").count()
    recently_updated_tasks = models.Task.objects.filter(
        assigned_to=current_user).order_by('-last_status_change')[:5]

    due_times = {
        str(task): time_until_due(task) for task in recently_updated_tasks}

    sprint_queryset = models.Sprint.objects.order_by('-start_sprint')[:10]

    sprint_list = []
    for sprint in sprint_queryset:
        sprint_list.append([sprint.name, sprint.productivity])
    sprint_json = json.dumps(sprint_list)

    status_overview = {
        'done_task_count': done_task_count,
        'done_project_percent': round(done_project_percent),
        'in_p_project_percent': round(in_p_project_percent),
        'behind_project_percent': round(behind_project_percent),
        'recently_updated_tasks': recently_updated_tasks,
        'due_times': due_times,
    }
    context = {
        'page_title': 'Dashboard',
        'work_count': work_count,
        'status_overview': status_overview,
        'productivity': productivity,
        'sprint_json': sprint_json,
    }
    return render(request, 'dashboard.html', context)


@login_required(login_url='login_user')
def user_projects(request, pk):
    # Compare the URL parameter with the currently logged-in user's identifier
    if request.user.pk != int(pk):
        pk = request.user.pk
        return redirect('user_projects', pk=pk)

    user_projects = models.Project.objects.filter(
        Q(owner=request.user.pk) | Q(collaborators=request.user.pk)).distinct()

    context = {
        'page_title': 'Projects',
        'user_projects': user_projects,
    }
    return render(request, 'user_projects.html', context)


@login_required(login_url='login_user')
def project_create(request, pk):
    # Compare the URL parameter with the currently logged-in user's identifier
    if request.user.pk != int(pk):
        pk = request.user.pk
        return redirect('user_projects', pk=pk)

    if request.method == 'POST':
        form = forms.ProjectCreateForm(request.POST)
        if form.is_valid():
            form.save()
            context = {'page_title': 'Projects', 'form': form}
            return render(request, 'project_create.html', context)

    form = forms.ProjectCreateForm
    context = {'page_title': 'Projects', 'form': form}
    return render(request, 'project_create.html', context)


@login_required(login_url='login_user')
def task_create(request):
    if request.method == 'POST':
        form = forms.TaskCreateForm(request.POST)
        if form.is_valid():
            form.save()
            context = {'page_title': 'Projects', 'form': form}
            return render(request, 'task_create.html', context)

    form = forms.TaskCreateForm
    context = {'page_title': 'Projects', 'form': form}
    return render(request, 'task_create.html', context)


@login_required(login_url='login_user')
def all_projects(request):
    all_projects = models.Project.objects.extra(
        select={'status_order': "status = 'IP'"},
        order_by=['-status_order', 'status']
    )
    context = {'page_title': 'Projects', 'all_projects': all_projects}
    return render(request, 'projects.html', context)


@login_required(login_url='login_user')
def project_details(request, pk):
    project = models.Project.objects.get(id=pk)
    project_tasks = models.Task.objects.filter(project=project)
    if request.method == 'POST':
        project_form = forms.ProjectCreateForm(request.POST, instance=project)

        if project_form.is_valid():
            project_form.save()
            context = {
                'page_title': 'Project details',
                'form': project_form,
                'tasks': project_tasks
            }
            return render(request, 'project_details.html', context)

    project_form = forms.ProjectCreateForm(instance=project)
    context = {
        'page_title': 'Project details',
        'form': project_form,
        'tasks': project_tasks
    }

    return render(request, 'project_details.html', context)


@login_required(login_url='login_user')
def task_details(request, pk):
    task = models.Task.objects.get(id=pk)

    if request.method == 'POST':
        task_form = forms.TaskCreateForm(request.POST, instance=task)
        if task_form.is_valid():
            task_form.save()
            context = {
                'page_title': 'Project details',
                'form': task_form
            }
            return render(request, 'task_details.html', context)

    task_form = forms.TaskCreateForm(instance=task)
    context = {
        'page_title': 'Project details',
        'form': task_form
    }
    return render(request, 'task_details.html', context)


@login_required(login_url='login_user')
def delete(request, pk, object):
    if object == 'task':
        delete_object = models.Task.objects.get(id=pk)
    elif object == 'project':
        delete_object = models.Project.objects.get(id=pk)

    if request.method == 'POST':
        delete_object.delete()
        return redirect('user_dashboard')

    context = {'page_title': 'Delete a task', 'delete_object': delete_object}
    return render(request, 'delete.html', context)
