from django.urls import path
from dashboard import views


urlpatterns = [
    # Register and Login
    path('register/', views.register_user, name='register_user'),
    path('login/', views.login_user, name='login_user'),
    path('logout/', views.logout_user, name='logout_user'),
    path('confirm-registr/', views.confirm_registr, name='confirm_registr'),
    path('activate/<uidb64>/<token>', views.activate, name='activate'),

    # User Operations
    path('update_user/<str:pk>/', views.update_user, name='update_user'),

    # Dashboard app
    path('dash/<str:pk>/', views.user_dashboard, name='user_dashboard'),
    path('user_projects/<str:pk>/', views.user_projects, name='user_projects'),
    path('project_create/<str:pk>/', views.project_create, name='project_create'),
    path('task_create/', views.task_create, name='task_create'),
    path('projects/', views.all_projects, name='projects'),
    path('project_details/<str:pk>/', views.project_details, name='project_details'),
    path('task_details/<str:pk>/', views.task_details, name='task_details'),

    # Delete
    path('delete/<str:pk>/<str:object>', views.delete, name='delete')
]
