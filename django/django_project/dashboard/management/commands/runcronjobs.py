from django.core.management.base import BaseCommand
from dashboard import cron


class Command(BaseCommand):
    help = 'Run cronjobs'

    def add_arguments(self, parser):
        parser.add_argument("function_to_execute", nargs="+", type=str)

    def handle(self, *args, **options):
        if "cron_hello" in options["function_to_execute"]:
            self.stdout.write("Adding function 'hello' to crontab")
            cron.cron_hello()
        elif "send_productivity_to_user" in options["function_to_execute"]:
            self.stdout.write("Adding function 'send_productivity_to_user' to crontab")
            cron.send_productivity_to_user()
        else:
            self.stdout.write("Wrong argument/s provided.")
