from django.contrib import admin
from .models import Project, Task, Sprint


def get_owner(obj):
    return obj.owner.username


get_owner.short_description = 'Owner'


def get_collaborators(obj):
    return [c for c in obj.collaborators.all()]


get_collaborators.short_description = 'Collaborators'


class TaskInline(admin.TabularInline):
    model = Task
    fields = ('name', 'description', 'start_date', 'due_date', 'assigned_to')
    readonly_fields = ('name', 'description', 'start_date', 'due_date')
    extra = 0


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', get_owner, get_collaborators, 'start_date',
                    'due_date', 'status')
    list_filter = ('owner', 'collaborators', 'start_date')
    search_fields = ('name', 'description')

    inlines = [TaskInline]


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'get_assigned_to', 'start_date', 'due_date', 'status')
    list_filter = ('assigned_to', 'status')
    search_fields = ('name', 'assigned_to__username', 'description')

    def get_assigned_to(self, obj):
        return obj.assigned_to.username

    get_assigned_to.short_description = 'Assigned_to'


@admin.register(Sprint)
class SprintAdmin(admin.ModelAdmin):
    list_display = ('id', 'project', 'get_task', 'start_sprint', 'end_sprint')
    search_fields = ('project', 'task')

    def get_task(self, obj):
        return [t for t in obj.task.all()]

    get_task.short_description = 'Get_Task'
