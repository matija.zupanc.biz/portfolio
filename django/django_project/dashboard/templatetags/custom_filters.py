from django import template
from dashboard import models

register = template.Library()


@register.filter(name='completion_percentage')
def completion_percentage(project):
    tasks = models.Task.objects.filter(project=project)
    if tasks.count() > 0:
        completion_percentage = (
            tasks.filter(status='FN').count() / tasks.count()) * 100
        return round(completion_percentage, 2)
    return 0


@register.filter(name='collaborators_count')
def collaborators_count(project):
    return project.collaborators.count()
