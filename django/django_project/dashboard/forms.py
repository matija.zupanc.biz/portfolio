from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from dashboard import models


class CustomUserCreationForm(UserCreationForm):
    terms_checkbox = forms.BooleanField(required=True)
    css_class = 'form-control mb-3'

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        if not first_name:
            raise forms.ValidationError('Please enter your First Name.')
        return first_name

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')
        if not last_name:
            raise forms.ValidationError('Please enter your Last Name.')
        return last_name

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if not email:
            raise forms.ValidationError('Please enter your email address.')
        elif User.objects.filter(email=email).exists():
            raise forms.ValidationError('Email address is already in use.')
        return email

    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields + (
            'first_name', 'last_name', 'email', 'terms_checkbox'
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter first name'
        })
        self.fields['last_name'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter last name'
        })
        self.fields['username'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter usernaname'
        })
        self.fields['email'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter email',
        })
        self.fields['password1'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter password'
        })
        self.fields['password2'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Confirm password'
        })
        self.fields['terms_checkbox'].widget.attrs.update({
            'class': 'form-check-input'
        })


class CustomUserChangeForm(UserChangeForm):
    css_class = 'form-control mb-3'

    class Meta(UserCreationForm.Meta):
        css_class = 'form-control mb-3'

        model = User
        fields = (
            'first_name', 'last_name', 'username', 'email'
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter first name'
        })
        self.fields['last_name'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter last name'
        })
        self.fields['username'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter usernaname'
        })
        self.fields['email'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter email',
        })


class ProjectCreateForm(forms.ModelForm):
    css_class = 'form-control mb-3'

    class Meta:
        model = models.Project
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter Project name'
        })
        self.fields['description'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter Description'
        })
        self.fields['owner'].widget.attrs.update({
            'class': self.css_class,
        })
        self.fields['collaborators'].widget.attrs.update({
            'class': self.css_class,
        })
        self.fields['status'].widget.attrs.update({
            'class': self.css_class,
        })


class TaskCreateForm(forms.ModelForm):
    css_class = 'form-control mb-3'

    class Meta:
        model = models.Task
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if not self.instance.pk:
            self.fields['status'].widget.attrs['readonly'] = True
            self.fields['status'].help_text = 'This field cannot be modified.'

        self.fields['name'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter Task name'
        })
        self.fields['description'].widget.attrs.update({
            'class': self.css_class,
            'placeholder': 'Enter Description'
        })
        self.fields['assigned_to'].widget.attrs.update({
            'class': self.css_class,
        })
        self.fields['project'].widget.attrs.update({
            'class': self.css_class,
        })
        self.fields['status'].widget.attrs.update({
            'class': self.css_class,
        })
        self.fields['time_spent'].widget.attrs.update({
            'class': self.css_class,
        })
