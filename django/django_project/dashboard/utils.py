from django.utils import timezone


def time_until_due(task):
    '''
    Calculate time until tasks due_time
    '''
    now = timezone.now()
    time_difference = task.due_date - now

    if time_difference.days > 0:
        return f"Task due in {time_difference.days} days"
    elif time_difference.seconds >= 3600:
        hours = time_difference.seconds // 3600
        return f"Task due in {hours} hours"
    elif time_difference.seconds >= 60:
        minutes = time_difference.seconds // 60
        return f"Task due in {minutes} minutes"
    else:
        return "Task is due soon"
