#!/bin/bash

cd /django_app
source venv/bin/activate

/usr/local/bin/python manage.py runcronjobs send_productivity_to_user

echo "Script execution completed at $(date)"