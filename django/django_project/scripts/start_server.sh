#!/bin/bash

set -e

# Run migrations just in case
echo "Starting migration..."
python ./manage.py migrate
echo "Migration completed!"

# collect static if neccessary
echo "Collecting static assets..."
python ./manage.py collectstatic --no-input
echo "Collection complete!"

echo "Running server in ${SERVER_RUNMODE} mode.."

if [ "$SERVER_RUNMODE" == "celery" ]; then
  celery --app=django_project.celery beat --scheduler django_celery_beat.schedulers:DatabaseScheduler --loglevel=info &
  celery --app=django_project.celery worker --loglevel=info &
else
  echo "second" && python ./manage.py runserver 0.0.0.0:8001
fi

tail -f /dev/null