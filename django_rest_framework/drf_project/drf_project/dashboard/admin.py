from django.contrib import admin
from drf_project.dashboard import models


def get_owner(obj):
    return obj.owner.username


get_owner.short_description = 'Owner'


class TaskInline(admin.TabularInline):
    model = models.Task
    fields = ('name', 'description', 'start_date', 'due_date', 'assigned_to')
    readonly_fields = ('name', 'description', 'start_date', 'due_date')
    extra = 0


@admin.register(models.Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', get_owner, 'start_date', 'due_date')
    list_filter = ('owner', 'collaborators', 'start_date')
    search_fields = ('name', 'description')

    inlines = [TaskInline]


@admin.register(models.Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'get_assigned_to', 'start_date', 'due_date', 'status')
    list_filter = ('assigned_to', 'status')
    search_fields = ('name', 'assigned_to__username', 'description')

    def get_assigned_to(self, obj):
        return obj.assigned_to.username

    get_assigned_to.short_description = 'Assigned_to'


@admin.register(models.Sprint)
class SprintAdmin(admin.ModelAdmin):
    list_display = ('name', 'project', 'get_task', 'start_sprint',
                    'end_sprint')
    list_filter = ('task', 'start_sprint', 'end_sprint')
    search_fields = ('name', 'project', 'task')

    def get_task(self, obj):
        return obj.task.name

    get_task.short_description = 'Task'


@admin.register(models.CronSchedule)
class CronScheduleAdmin(admin.ModelAdmin):
    list_display = ('command', 'schedule', 'status', 'cron_command')
