from django.core.management.base import BaseCommand
from drf_project.dashboard import cron


class Command(BaseCommand):
    help = 'Run cronjobs'

    def add_arguments(self, parser):
        parser.add_argument("function_to_execute", nargs="+", type=str)

    def handle(self, *args, **options):
        if "cron_hello" in options["function_to_execute"]:
            self.stdout.write("Running function 'hello'")
            cron.cron_hello()
        elif "send_productivity_to_user" in options["function_to_execute"]:
            self.stdout.write("Running function 'send_productivity_to_user'")
            cron.send_productivity_to_user()
        else:
            self.stdout.write("Wrong argument/s provided.")
