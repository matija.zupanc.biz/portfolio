from django.contrib.auth.models import User, Group
from rest_framework import serializers
from drf_project.dashboard import models


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Project
        fields = ['url', 'name', 'description', 'start_date', 'due_date',
                  'owner', 'collaborators', 'status']


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Task
        fields = ['url', 'name', 'description', 'start_date', 'due_date',
                  'status', 'assigned_to', 'project', 'productivity']


class SprintSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Sprint
        fields = ['url', 'name', 'project', 'task', 'start_sprint',
                  'end_sprint', 'productivity']


class CronScheduleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.CronSchedule
        fields = ['url', 'command', 'schedule', 'status', 'cron_command']
