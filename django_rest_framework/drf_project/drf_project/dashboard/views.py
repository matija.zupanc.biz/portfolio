# from django.shortcuts import render
from django.contrib.auth.models import User, Group
from django.db.models import Q
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from drf_project.dashboard import serializers, models, utils

from crontab import CronTab


class UserViewset(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = serializers.UserSerializer
    permission_classes = [permissions.IsAuthenticated]

    # This is for my own practice and extended integration in later stages
    def perform_create(self, serializer):
        serializer.save(is_active=False)


class GroupViewset(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = serializers.GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProjectViewset(viewsets.ModelViewSet):
    queryset = models.Project.objects.all().order_by('-due_date')
    serializer_class = serializers.ProjectSerializer
    permission_classes = [permissions.IsAuthenticated]
    # http_method_names = ['get', 'head']


class UserProjectViewset(viewsets.ModelViewSet):
    serializer_class = serializers.ProjectSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return models.Project.objects.filter(
            Q(owner=user) | Q(collaborators=user)
        ).distinct().order_by('-due_date')


class TaskViewset(viewsets.ModelViewSet):
    queryset = models.Task.objects.all().order_by('-due_date')
    serializer_class = serializers.TaskSerializer
    permission_classes = [permissions.IsAuthenticated]


class SprintViewset(viewsets.ModelViewSet):
    queryset = models.Sprint.objects.all().order_by('start_sprint')
    serializer_class = serializers.SprintSerializer
    permission_classes = [permissions.IsAuthenticated]


class DashboardViewset(viewsets.ViewSet):
    permission_classes = [permissions.IsAuthenticated]

    def list(self, request):
        user = self.request.user

        # Query the models:

        # Retrieve all tasks associated with the user
        tasks = models.Task.objects.filter(assigned_to=user)

        # Project productivity
        if tasks:
            project_productivity = tasks[0].productivity
        else:
            project_productivity = 0

        # Retrieve Total count of projects associated with user
        total_projects = models.Project.objects.filter(
            Q(owner=user) | Q(collaborators=user)
        ).distinct()

        # Retrieve all projects associated with the user
        owned_projects_count = total_projects.filter(
            owner=user).count()
        collab_projects_count = total_projects.filter(
            collaborators=user).exclude(owner=user).count()

        ''' Query that finds all the users who are collaborators on projects
        owned by the user or users who own projects where the specified user
        is a collaborator and then counts the number of distinct users in the
        result, excluding the user themselves.'''
        collaborators_count = User.objects.filter(
            Q(collaborating_projects__in=user.owned_projects.all()) |
            Q(owned_projects__collaborators=user)
        ).exclude(id=user.id).distinct().count()

        # Retrieve statuses - Projects:
        # Done
        done_project_count = total_projects.filter(status="CM").count()
        # In progress
        in_p_project_count = total_projects.filter(status="IP").count()
        # Behind
        behind_project_count = total_projects.filter(status="BH").count()

        # Calculate project status percentages
        done_project_percent, in_p_project_percent, behind_project_percent = (
            utils.project_status_percentages(
                total_projects.count(),
                done_project_count,
                in_p_project_count,
                behind_project_count)
        )

        # Retrieve statuses - Tasks:
        # Done
        done_task_count = tasks.filter(status="FN").count()

        # Old way but here for reference
        # unfinished_tasks = models.Task.objects.filter(
        #     Q(assigned_to=user) & Q(status="YS") |
        #     Q(assigned_to=user) & Q(status="IP")
        # )

        # Unfinished
        # New way since we already have "tasks" queryset
        unfinished_tasks = tasks.filter(Q(status="YS") | Q(status="IP"))

        due_times = [
            {str(task): utils.time_until_due(task)}
            for task in unfinished_tasks
        ]

        # Retrieve 10 Sprint instances with the newest "start_sprint" field
        sprint = models.Sprint.objects.all().order_by('-start_sprint')[:10]

        # Get the desired fields ('name' and 'productivity')
        sprint_productivity = [
            {
                'name': sprint_data.name,
                'productivity': sprint_data.productivity
            } for sprint_data in sprint
        ]

        # Create a response with the desired fields
        data = {
            'total_projects_count': total_projects.count(),
            'owned_projects_count': owned_projects_count,
            'collab_projects_count': collab_projects_count,
            'project_productivity': project_productivity,
            'tasks_count': tasks.count(),
            'collaborators_count': collaborators_count,
            'done_project_percent': done_project_percent,
            'in_progress_project_percent': in_p_project_percent,
            'behind_project_percent': behind_project_percent,
            'done_task_count': done_task_count,
            'due_times': due_times,
            'sprint_productivity': sprint_productivity
        }
        return Response(data)


class CronScheduleViewset(viewsets.ModelViewSet):
    queryset = models.CronSchedule.objects.all()
    serializer_class = serializers.CronScheduleSerializer
    permission_classes = [permissions.IsAuthenticated]
