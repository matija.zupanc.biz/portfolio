from django.apps import AppConfig


class DashboardConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'drf_project.dashboard'

    def ready(self):
        import drf_project.dashboard.signals
