from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from drf_project.dashboard import models

from crontab import CronTab


@receiver(post_save, sender=models.Sprint)
def generate_name(sender, instance, created, **kwargs):
    if created and not instance.name:
        # Generate the name
        generated_name = 'Sprint' + ' ' + str(instance.id)
        instance.name = generated_name
        instance.save()


@receiver(post_save, sender=models.CronSchedule)
def generate_cron_command(sender, instance, **kwargs):
    # Create a new CronTab object
    cron = CronTab(user='root')
    # cron = CronTab(user='matija')

    # Check if a cronjob with the same id already exists
    existing_cron_command = list(cron.find_comment(str(instance.id)))

    if existing_cron_command:
        # Update the existing cronjob
        existing_cron_command[0].setall(instance.schedule)
        existing_cron_command[0].set_command(instance.cron_command)
        # Enable or disable cronjob
        if instance.status == 'AC':
            existing_cron_command[0].enable()
        else:
            existing_cron_command[0].enable(False)
        cron.write()
    else:
        # Create a new cronjob
        new_cron_command = cron.new(
            command=instance.cron_command,
            comment=str(instance.id),
        )
        new_cron_command.setall(instance.schedule)
        # Enable or disable cronjob
        if instance.status == 'AC':
            new_cron_command.enable()
        else:
            new_cron_command.enable(False)
        cron.write()


@receiver(post_delete, sender=models.CronSchedule)
def delete_cron_command(sender, instance, **kwargs):
    cron = CronTab(user='root')
    # cron = CronTab(user='matija')

    # Find cronjob with the corresponding id
    find_cron_command = list(cron.find_comment(str(instance.id)))
    # If cronjob exists delete it
    if find_cron_command:
        cron.remove(find_cron_command)
        cron.write()
