from django.utils import timezone


def project_status_percentages(
        total_projects_count=0,
        done_project_count=0,
        in_p_project_count=0,
        behind_project_count=0
):
    '''
    Calculating Project status percentages
    '''
    if total_projects_count == 0:
        done_project_percent = 0
        in_p_project_percent = 0
        behind_project_percent = 0
    else:
        done_project_percent = (
            (done_project_count / total_projects_count) * 100
        )
        in_p_project_percent = (
            (in_p_project_count / total_projects_count) * 100
        )
        behind_project_percent = (
            (behind_project_count / total_projects_count) * 100
        )
    return done_project_percent, in_p_project_percent, behind_project_percent


def time_until_due(task):
    '''
    Calculate time until tasks due_time
    '''
    now = timezone.now()
    time_difference = task.due_date - now

    if time_difference.days > 0:
        return f"Task due in {time_difference.days} days"
    elif time_difference.seconds >= 3600:
        hours = time_difference.seconds // 3600
        return f"Task due in {hours} hours"
    elif time_difference.seconds >= 60:
        minutes = time_difference.seconds // 60
        return f"Task due in {minutes} minutes"
    else:
        return "Task is due soon"
