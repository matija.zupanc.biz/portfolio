from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Project(models.Model):
    STATUS_CHOICES = (
        ("PG", "Pending"),
        ("IP", "In progress"),
        ("CM", "Completed"),
        ("BH", "Behind"),
    )
    name = models.CharField(max_length=100)
    description = models.TextField()
    start_date = models.DateField(default=timezone.now().date())
    due_date = models.DateField()
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='owned_projects')
    collaborators = models.ManyToManyField(
        User, related_name='collaborating_projects')
    status = models.CharField(
        max_length=20, choices=STATUS_CHOICES, default="PG")

    def __str__(self):
        return self.name


class Task(models.Model):
    STATUS_CHOICES = (
        ("YS", "Yet to start"),
        ("IP", "In progress"),
        ("IR", "In review"),
        ("FN", "Finished"),
    )

    name = models.CharField(max_length=100)
    description = models.TextField()
    start_date = models.DateTimeField(default=timezone.now)
    due_date = models.DateTimeField()
    status = models.CharField(
        max_length=20, choices=STATUS_CHOICES, default="YS")
    assigned_to = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='user_tasks')
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name='tasks', null=True)
    timestamp = models.DateTimeField(auto_now=True)
    last_status_change = models.DateTimeField(null=True, editable=False)
    time_spent = models.DurationField(null=True, blank=True)

    class Meta:
        indexes = [
            models.Index(fields=['assigned_to']),
        ]

    def __str__(self):
        return self.name

    @property
    def productivity(self):
        total_tasks = Task.objects.filter(assigned_to=self.assigned_to).count()
        completed_tasks = Task.objects.filter(
            assigned_to=self.assigned_to, status="FN").count()

        if total_tasks > 0:
            productivity = round((completed_tasks / total_tasks) * 100, 2)
        else:
            productivity = 0

        return productivity


class Sprint(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name='project_sprint')
    task = models.ManyToManyField(Task, related_name='task_sprint')
    start_sprint = models.DateTimeField(default=timezone.now)
    end_sprint = models.DateTimeField()

    @property
    def productivity(self):
        completed_tasks = self.task.filter(status="FN").count()
        if self.task.count() > 0:
            productivity = (completed_tasks / self.task.count()) * 100
        else:
            productivity = 0

        return productivity


class CronSchedule(models.Model):
    STATUS_CHOICES = (
        ("AC", "Active"),
        ("IC", "Inactive"),
    )
    command = models.CharField(max_length=100, null=True, blank=True, help_text="One command should be 'cron_hello' and the other should be 'send_productivity_to_user'.")
    schedule = models.CharField(max_length=50, null=True, blank=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default="IC")

    def __str__(self):
        return self.command

    @property
    def cron_command(self):
        full_command = 'bash /django_app/scripts/start_cronjobs.sh ' + self.command + ' 1>/django_app/logs/cron.log 2>/django_app/logs/cron_error.log'

        return full_command
