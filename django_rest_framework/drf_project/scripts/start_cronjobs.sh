#!/bin/bash

cd /django_app
source venv/bin/activate

cron_function="$1"

/usr/local/bin/python manage.py runcronjobs $cron_function

echo "Script execution completed at $(date)"
