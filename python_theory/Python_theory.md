[Python theory trello](https://trello.com/b/39RCMklS/matija)

# 1. What is Python?

Python is a high-level, interpreted programming language (used for a wide variety of applications. It was created by Guido van Rossum and first released in 1991.)

Python is known for its readability, ease of use, and clean syntax, which makes it a popular choice among developers for both small and large-scale projects. It is also an open-source language, which means that it is free to use and modify, and has a large and active community of developers who contribute to its development and maintenance. Python is used in many areas of computing, including web development, data science, machine learning, scientific computing, and more.

# 2. What are the benefits of using Python?

## 2.a General

* Readability: Python's syntax is clean and easy to read, which makes it easier to write and maintain code.

* Portability: Python code can run on a wide range of platforms, including Windows, macOS, Linux, and more.

* Flexibility: Python can be used for a wide range of tasks, from web development to data science to machine learning and more.

* Large standard library: Python comes with a large and comprehensive standard library, which makes it easier to accomplish common programming tasks without having to write a lot of code.

* Third-party libraries: Python has a large and active community of developers who have created many useful third-party libraries and tools that can be easily installed and used.

* Interpreted: Python is an interpreted language, which means that code can be executed immediately without the need for compilation.

* Dynamic: Python is a dynamically typed language, which means that variable types are determined at runtime, making it more flexible than statically typed languages.

* Open-source: Python is an open-source language, which means that it is free to use and modify, and has a large and active community of developers who contribute to its development and maintenance.

## 2.b Django and web develop (Specific benefits of using Python, and the Django framework in particular, for web development)

* Rapid development: Python's clean and concise syntax, as well as the large number of third-party libraries available, make it possible to rapidly develop complex web applications.

* Scalability: Django is designed to be scalable, making it possible to handle large amounts of traffic and data without sacrificing performance.

* Security: Django comes with built-in security features, such as protection against common attacks like SQL injection and cross-site scripting (XSS).

* Versatility: Django can be used for a wide variety of web development tasks, including building content management systems (CMSs), social networks, e-commerce sites, and more.

* Object-relational mapping (ORM): Django's ORM makes it easy to work with databases, allowing developers to write database queries using Python instead of SQL.

* Built-in admin interface: Django comes with a built-in admin interface, which makes it easy to manage data and content in your web application without having to write a lot of code.

* Community: Django has a large and active community of developers who contribute to its development and maintenance, as well as creating useful third-party packages and tools.

* Documentation: Django has excellent documentation, making it easy to get started and learn new features as you go.

# 3. How is Python an interpreted language?

Python is an interpreted language because it does not need to be compiled before it is executed. Instead, the Python interpreter reads and executes the code directly, line by line, as it encounters it.

When a Python program is run, the interpreter first parses the code into an abstract syntax tree (AST), which is a high-level representation of the code's structure. Then, the interpreter executes the code, one statement at a time, by traversing the AST.

This is in contrast to compiled languages like C or C++, where the source code is compiled into machine code before it is executed. In a compiled language, the compiler translates the entire source code into an executable file, which can be run directly on the computer's processor.

The fact that Python is an interpreted language has both advantages and disadvantages. On the one hand, it makes Python programs easier to write and debug, since code can be executed immediately without the need for compilation. On the other hand, interpreted languages are generally slower than compiled languages, since the interpreter needs to execute the code at runtime rather than running pre-compiled machine code. However, modern implementations of Python (such as PyPy) have made significant performance improvements.

# 4. What is PEP 8 and why is it important?

PEP 8 (Python Enhancement Proposal 8) is a set of guidelines for writing Python code. It was created by Guido van Rossum, the creator of Python, and is intended to promote code readability and consistency among Python projects.

PEP 8 covers a wide range of topics, including naming conventions, code layout, indentation, comments, and more. Some of the key principles of PEP 8 include:

* Use of whitespace: PEP 8 recommends using whitespace consistently to improve code readability. This includes using spaces instead of tabs for indentation, using blank lines to separate code blocks, and placing spaces around operators and after commas in function calls.

* Naming conventions: PEP 8 recommends using consistent naming conventions for variables, functions, and classes. For example, variable names should be lowercase with words separated by underscores, while class names should use CamelCase notation.

* Code structure: PEP 8 recommends using a consistent code structure, including how to organize imports, how to order functions and classes within a file, and how to use comments to explain code.

* Best practices: PEP 8 recommends following best practices for writing Python code, such as avoiding long lines of code, using the built-in functions and modules, and adhering to the "flat is better than nested" principle.

Following PEP 8 is important because it promotes consistency and readability across Python projects, which makes it easier for developers to read and understand code written by others, and to collaborate on large projects. It also helps to reduce the likelihood of errors and bugs caused by inconsistent coding practices. Many popular Python libraries and frameworks, such as Django and Flask, adhere to PEP 8 guidelines, and many development tools and editors provide support for checking code against PEP 8 standards.

# 5. What is a virtual environment in python?

A virtual environment in Python is a way to create an isolated environment where you can install Python packages without affecting the global Python installation or other virtual environments.

When you create a virtual environment, you create a new directory that contains a self-contained Python installation, along with a copy of the pip package manager. This allows you to install Python packages specific to your project, without worrying about conflicts with other projects or the global Python installation.

Once you have activated the virtual environment, you can use pip to install packages specific to your project, and they will be installed within the virtual environment, isolated from other Python installations.

Using virtual environments is important for Python developers because it allows you to manage package dependencies and project-specific configurations more easily. It also helps to ensure that your project can be easily reproduced on other machines or by other developers, since all of the dependencies are contained within the virtual environment.

# 6. What are the different data types in python?

**Numeric types:**

int: Integers (whole numbers), such as 1, -5, 100, etc.

float: Floating-point numbers, such as 3.14, -2.5, 1.0, etc.

complex: Complex numbers, such as 3 + 4j, -1 - 0.5j, etc.

**Boolean type:**

bool: Boolean values, which can be either `True` or `False`.

**Sequence types:**

str: Strings, which are sequences of characters enclosed in quotes, such as "Hello", 'Python', etc.

list: Lists, which are ordered sequences of objects enclosed in square brackets, such as `[1, 2, 3]`, `['apple', 'banana', 'orange']`, etc.

tuple: Tuples, which are ordered sequences of objects enclosed in parentheses, such as `(1, 2, 3)`, `('apple', 'banana', 'orange')`, etc.

**Set types:**

set: Sets, which are unordered collections of unique objects enclosed in curly braces, such as `{1, 2, 3}`, `{'apple', 'banana', 'orange'}`, etc.

frozenset: Immutable sets, which are like sets but cannot be modified after creation.

**Mapping type:**

dict: Dictionaries, which are unordered collections of key-value pairs enclosed in curly braces and separated by colons, such as `{'name': 'John', 'age': 30}`, `{'apple': 1.0, 'banana': 2.5, 'orange': 3.0}`, etc.

# 7. What is the difference between a list and a tuple in Python?

* Mutability: Lists are mutable, which means you can add, remove, or modify elements after creating the list. Tuples, on the other hand, are immutable, which means you cannot modify the elements after creating the tuple. If you need to modify a tuple, you have to create a new tuple with the modified elements.

* Syntax: Lists are created using square brackets [], and elements are separated by commas. Tuples are created using parentheses (), and elements are also separated by commas. However, parentheses are optional in some cases, and you can create a tuple without using parentheses by separating the elements with commas.

* Usage: Lists are typically used for collections of items that need to be modified, such as a list of tasks or a list of items in a shopping cart. Tuples are typically used for collections of items that are fixed and not intended to be modified, such as a pair of coordinates or a date and time.

* Memory usage: Lists require more memory than tuples, because they are mutable and therefore need extra space to store their internal state. Tuples are smaller and more memory-efficient than lists, especially for large collections of data.

* Speed: In general, tuples are faster than lists for certain operations, such as indexing and iteration, because they are simpler and require less memory.

# 8. What is the difference between a set and a dictionary in Python?

* Purpose: Sets are used to store a collection of unique items, while dictionaries are used to store key-value pairs. In other words, sets are used to represent a group of distinct elements, while dictionaries are used to represent a set of mappings between keys and values.

* Syntax: Sets are created using curly braces {}, or the set() constructor function, and elements are separated by commas. Dictionaries are created using curly braces {}, or the dict() constructor function, and each element is a key-value pair separated by a colon :.

* Indexing: Sets are not indexable, which means you cannot access individual items by their position in the set. Instead, you can iterate over the set using a for loop or use built-in set methods like pop() or remove() to access and remove elements. Dictionaries, on the other hand, are indexed by keys, which means you can access values by their corresponding keys.

* Duplication: Sets do not allow duplicate items, while dictionaries do not allow duplicate keys. If you try to add a duplicate item to a set, it will be ignored. If you try to add a duplicate key to a dictionary, the new value will overwrite the existing value associated with that key.

* Values: In a dictionary, the values can be of any data type, while in a set, all the elements must be hashable (immutable and uniquely identifiable).

In general, if you need to store a collection of unique items and do not care about their order or position, use a set. If you need to store a collection of key-value pairs and want to be able to access the values by their keys, use a dictionary.

# 9. What are decorators in Python?

## 9.a Theory

In Python, a decorator is a function that takes another function as input, and returns a modified version of the original function. The decorator can add functionality to the original function, modify its behavior, or perform some other action before or after the function is called.

Decorators are often used to simplify the code by reducing code duplication, making it more readable and maintainable. They allow you to separate concerns by defining reusable functions that can be applied to other functions without changing their source code.

## 9.b Decorator examples (Most common)

More EX on Trello board

# 10. What is the difference between `is` and `==` in Python?

The `==` operator checks if two objects have the same value. It compares the contents of the objects to see if they are equal.

On the other hand, the `is` operator checks if two objects are the same object. It checks if the objects are located in the same memory address.

# 11. What is the purpose of the `__init__` method in Python classes?

The `__init__` method is a special method in Python classes that is called when an instance of the class is created. It is commonly used to initialize the attributes of the object.

The `__init__` method makes it easier and more convenient to create and initialize new objects. It also makes the code more readable and easier to understand, since the initialization code is grouped together in one place.

# 12. What is the difference between instance, class and static methods in Python?

In Python, instance, class, and static methods are different ways of defining methods in a class.

* Instance methods: These are the most common type of method in Python classes. Instance methods are called on an instance of the class, and they have access to the instance's attributes. Instance methods are defined with the self parameter, which refers to the instance that the method is being called on.

* Class methods: These methods are called on the class itself, rather than on an instance of the class. Class methods are defined with the `@classmethod` decorator, and they take the class as their first argument (usually called cls). Class methods can be used to modify class-level attributes, or to create new instances of the class.

* Static methods: These methods are not tied to either the instance or the class, and they don't have access to any instance or class attributes. Static methods are defined with the @staticmethod decorator, and they don't take any special arguments like self or cls. Static methods are often used as utility functions that are related to the class, but don't depend on any instance or class state.

# 13. What are lambda functions in Python?

Lambda function is a small anonymous function that can take any number of arguments, but can only have one expression. The expression is evaluated and returned as the function's result. The main advantage of using a lambda function is that it allows you to create small, one-line functions on the fly, without the need to define a formal function using the def keyword.

# 14. What is the purpose of `*args` and `**kwargs` in Python function definitions?

`*args` and `**kwargs` are special syntax used in function definitions to allow for a variable number of arguments to be passed to the function.

The `*args` syntax is used to pass a variable number of positional arguments to a function. It allows the function to accept any number of arguments, which are then treated as a tuple inside the function.

The `**kwargs` syntax is used to pass a variable number of keyword arguments to a function. It allows the function to accept any number of arguments, which are then treated as a dictionary inside the function.

The `*args` and `**kwargs` syntax are often used together in Python functions to create more flexible and versatile functions that can accept both positional and keyword arguments.

# 15. What is the difference between a shallow copy and a deep copy in Python?

A shallow copy creates a new object that is a reference to the original object. This means that any changes made to the new object will also be reflected in the original object, and vice versa. A shallow copy can be created using the copy() method or the slicing operator `[:]`.

However there is a difference between adding objects to list and changing objects

A deep copy, on the other hand, creates a new object that is a copy of the original object, but with all nested objects also copied recursively. This means that changes made to the new object will not affect the original object, and vice versa. A deep copy can be created using the `deepcopy()` method from the copy module.

# 16. What are generators in Python?

Generators in Python are a type of iterable, like lists or tuples. However, unlike lists or tuples, generators do not store all the values in memory at once. Instead, they generate the values on the fly as they are requested.

Generators are created using a special type of function called a generator function. A generator function is defined like a regular function, but instead of using the return keyword to return a value, it uses the yield keyword to yield a value.

Generators are particularly useful when working with large datasets or when dealing with an unknown amount of data. Since generators only generate the values as they are needed, they can be more memory-efficient than lists or tuples, which can store all the values in memory at once.

# 17. What is the purpose of the `yield` keyword in Python?

The `yield` keyword in Python is used in function definitions to create a generator. When a function contains the `yield` keyword, it becomes a generator function that returns an iterator object, rather than a regular function that returns a value and then exits.

Each time the `yield` keyword is encountered in a generator function, it "pauses" the function and returns the current value to the iterator object. The next time the generator function is called, it resumes execution from where it left off, picking up where it left off and generating the next value in the sequence.

In summary, the purpose of the `yield` keyword is to allow you to generate a sequence of values on-the-fly, using a generator function, and to return those values one at a time to an iterator object.

# 18. What is a context manager in Python?

A context manager in Python is an object that is used to manage resources in a way that ensures they are properly and automatically cleaned up when they are no longer needed. It allows the programmer to define a setup code and a teardown code for the resource in a way that is intuitive and easy to use.

A context manager is typically used with the `with` statement in Python, which ensures that the setup code is executed at the beginning of the block and the teardown code is executed at the end of the block. The `with` statement creates a temporary scope and ensures that any resources created in that scope are properly cleaned up when the scope is exited.

Context managers are especially useful when working with resources that need to be cleaned up or released when they are no longer needed, such as files, network connections, or locks. By using a context manager, the programmer can avoid common problems such as resource leaks and race conditions.

In Python, context managers can be implemented in two ways: as a class with `enter()` and `exit()` methods, or as a function decorated with `@contextmanager`. The `enter()` method is called when the block is entered, and the `exit()` method is called when the block is exited. The `@contextmanager` decorator creates a generator function that yields a context object, and the code before the `yield` statement is executed when the block is entered, and the code after the `yield` statement is executed when the block is exited.

# 19. What is the purpose of the `with` statement in Python?

The primary purpose of the `with` statement is to ensure that a resource is properly managed and cleaned up, even if an exception occurs while the code block is executing. By using a context manager and the `with` statement, you can ensure that the necessary cleanup actions are taken regardless of whether the code block executes successfully or not.

In addition to providing a way to manage resources, the `with` statement can also be used to implement other common patterns, such as locking and transactions. For example, in a multi-threaded application, you might use a context manager to acquire a lock before executing a critical section of code and release the lock after the critical section is complete.

Overall, the `with` statement is a powerful tool in Python that can be used to simplify the management of resources, implement common patterns, and ensure that code is executed in a safe and predictable way.

# 20. How can you handle exceptions in Python?

In Python, exceptions can be handled using the `try-except` block. The try block contains the code that might raise an exception, and the except block is used to catch and handle the exception.

You can also use the `finally` block to specify some code that should be executed whether or not an exception occurred. The `finally` block is executed after the try and except blocks, regardless of whether an exception occurred.

This is useful for tasks like closing files or network connections, which should be done regardless of whether the code raised an exception or not.

# 21. What is Memoization in programming?

Memoization is an optimization technique used in computer programming to speed up the execution of a function that is repeatedly called with the same arguments. The basic idea of memoization is to cache the results of function calls and return the cached result when the same inputs occur again.

In other words, memoization is a way to store the output of a function for a given set of input arguments so that if the function is called again with the same arguments, the cached result can be returned without having to recompute the result. This can significantly improve the performance of functions that are called frequently with the same arguments.

# 22. What is the difference between `append` and `extend` in Python?

`append()` is used to add a single element to the end of the list. The single element can be of any type, including another list. When you append another list to an existing list, the list is added as a single element to the end of the list.

`extend()` is used to add multiple elements to the end of the list. The argument to `extend()` must be an iterable, such as a list, tuple, string, or range. When you extend a list with another list, the elements of the second list are added individually to the end of the first list.

# 23. What is the difference between a module and a package in Python?

In Python, a module is a single file that contains Python code, while a package is a collection of modules placed in a directory with a special `__init__.py` file.

A module is a file containing Python definitions and statements. It is a way of organizing code and making it reusable. A module can define functions, classes, and variables, and can also include runnable code. Modules are imported into other Python code using the import statement.

A package is a collection of related modules. It is organized as a directory containing an `__init__.py` file and one or more module files. The `__init__.py` file is executed when the package is imported and can define variables, functions, and classes that will be available to the user when they import the package. Packages can also contain sub-packages, which are simply other directories containing an `__init__.py` file and other modules.

In summary, a module is a single file containing Python code, while a package is a collection of modules organized in a directory hierarchy.

# 24. How do you use the map function in Python?

The `map()` function is used to apply a given function to each element of an iterable and return a new iterable with the results.

The `map()` function returns a map object which can be converted into a list, tuple, or set using the corresponding function.

# 25. How do you use the filter function in Python?

In Python, the `filter()` function is used to filter out the elements from an iterable that don't satisfy a certain condition, based on a function that is passed as an argument.

# 26. What is threading in Python?

Threading in Python refers to the ability of a program to handle multiple threads of execution simultaneously. A thread is a lightweight process that can run in parallel with other threads, allowing programs to perform multiple tasks concurrently. Python's threading module provides a way to create and manage threads in a program.

Threads in Python share the same memory space and can access the same data, which makes them useful for implementing concurrent programs. However, this also introduces the potential for race conditions and other concurrency issues, which must be carefully managed in order to avoid bugs and other problems.
