# [Http](https://trello.com/c/4S7CtEkj/267-how-the-protocol-is-used-in-general)

## How the protocol is used in general?

HTTP (Hypertext Transfer Protocol) is a protocol that enables communication between clients and servers over the internet. It follows a client-server model, where the client initiates a request to the server, and the server responds with a corresponding response.

General Usage:

1. **Establishing Connections**: HTTP typically uses TCP (Transmission Control Protocol) to establish a reliable connection between the client and server. It follows a request-response model, where each request is independent of others.

2. **Stateless Protocol**: HTTP is stateless, meaning each request-response cycle is independent. The server does not maintain any information about past requests from a client.

3. **URL and Resources**: HTTP uses Uniform Resource Locators (URLs) to specify the location of resources on the web. Clients make requests to these URLs to retrieve or modify resources.

4. **HTTP Methods**: Clients use various HTTP methods to specify the action they want to perform on a resource. Common methods include GET (retrieve), POST (create), PUT (update), DELETE (remove), and more.

5. **Status Codes**: HTTP responses include status codes to indicate the outcome of a request. These codes range from informational (1xx) to successful (2xx), redirection (3xx), client errors (4xx), and server errors (5xx).

6. **Hyperlinks and Navigation**: HTTP facilitates linking between resources through hyperlinks. Clients can navigate the web by following links embedded in HTML or other response formats.

7. **Extensibility**: HTTP allows the addition of custom headers and methods, enabling the development of specialized functionalities and protocols built on top of the base HTTP.

## Status codes

1. Informational responses (100 - 199)

2. Successful responses (200 - 299)

3. Redirection messages (300 - 399)

4. Client error responses (400 - 499)

5. Server error responses (500 - 599)

Some of the more important are:

* **100 (Continue)**: This status code is used to indicate that the initial part of the request has been received and the server is willing to proceed with the client's request.

* **200 (OK)**: This status code indicates that the request was successful, and the server has returned the requested resource. It is the most commonly used status code for successful responses.

* **201 (Created)**: This code is used when a new resource has been successfully created as a result of the request. It is often returned after a successful POST request.

* **204 (No Content)**: This code indicates that the server successfully processed the request but does not need to return any content in the response body. It is commonly used for successful DELETE or PUT requests.

* **301 (Moved Permanently)**: This code is used to indicate that the requested resource has been permanently moved to a new location. Clients should update their bookmarks or links accordingly.

* **302 (Found)**: This status code is similar to 301, but it indicates that the resource has been temporarily moved to a different location. Clients should continue to use the original URL for future requests.

* **307 (Temporary Redirect)**: Similar to 302, this code indicates a temporary redirect, but the client should use the original request method when resending the request to the new location.

* **308 (Permanent Redirect)**: This code is similar to 301, but it instructs the client to use the new URL for all future requests.

* **400 (Bad Request)**: This status code indicates that the server could not understand or process the request due to malformed syntax or invalid parameters. It is often used when the client sends incorrect or incomplete data.

* **401 (Unauthorized)**: This code indicates that the requested resource requires authentication, and the client needs to provide valid credentials (such as username and password) to access it.

* **403 (Forbidden)**: This status code indicates that the server understands the request, but the client does not have the necessary permissions to access the requested resource.

* **404 (Not Found)**: This code indicates that the requested resource could not be found on the server. It is commonly used when the URL or resource path is invalid or does not exist.

* **500 (Internal Server Error)**: This status code indicates that an unexpected error occurred on the server while processing the request. It is a generic error response used when the server encounters an internal issue.

* **503 (Service Unavailable)**: This code indicates that the server is temporarily unable to handle the request, usually due to being overloaded or undergoing maintenance. It suggests that the client should try again later.

## Communication flow representation

[URL] --> [Request Method] --> [Request Headers] --> [Request Body]
[URL] <-- [Status Code] <-- [Response Headers] <-- [Response Body]

## Parts of the HTTP protocol

1. **URL (Uniform Resource Locator)**: A URL is used to identify and locate resources on the web. It consists of several parts:

    * `http://www.example.com:80/path/to/resource?param1=value1&param2=value2#fragment`

      * Scheme: Specifies the protocol used, such as "http://" in the example above.

      * Host: Indicates the domain or IP address of the server, such as " ".

      * Port: (optional) Specifies the port number to connect to, such as ":80" for HTTP (default port).

      * Path: Represents the specific resource on the server, such as "/path/to/resource".

      * Query Parameters: (optional) Contains key-value pairs passed to the server, such as "?param1=value1&param2=value2".

      * Fragment: (optional) Identifies a specific section within the resource, such as "#fragment".

2. **Request Methods**: HTTP defines several request methods to specify the action to be performed on a resource.

    * **GET**: Requests the specified resource from the server.

    * **POST**: Submits data to be processed by the server.

    * **PUT**: Updates an existing resource with the provided data.

    * **DELETE**: Removes the specified resource.

3. **Request Headers**: HTTP requests include headers to provide additional information to the server. Here's an example of request headers:

    * `GET /api/users/1 HTTP/1.1`
    * `Host: http://example.com`
    * `User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36`
    * `Accept-Language: en-US,en;q=0.9`

      * Request Line: The request line specifies the HTTP method (GET), the target URL or path (/api/users/1), and the HTTP version (HTTP/1.1).

      * Request Headers: In this example, the headers include the Host header specifying the domain (`http://example.com/`), User-Agent header specifying the client's browser details, and Accept-Language header specifying the preferred language.

4. **Response Headers**: HTTP responses also include headers to provide information about the response. Here's an example of response headers:

    * `HTTP/1.1 200 OK`
    * `Content-Type: application/json`
    * `Content-Length: 87`

      * Content-Type: Specifies the type of data in the response, such as JSON, HTML, or plain text.

      * Content-Length: Indicates the length of the response body in bytes.

5. **Response Body**: The response body contains the actual data sent by the server. It can be in various formats, such as JSON, HTML, XML, or binary. Here's an example of a JSON response body:

    * `{`
    * `"id": 1,`
    * `"name": "John Doe",`
    * `"email": "john@example.com"`
    * `}`

## Curl examples for HTTP methods

1. **GET**:

    * `curl -X GET https://example.com/api/users/1`

2. **POST**:

    * `curl -X POST https://example.com/api/users -H "Content-Type: application/json" -d '{`
    * `"name": "John Doe",`
    * `"email": "john@example.com"`
    * `}'`

3. **PUT**:

    * `curl -X PUT https://example.com/api/users/1 -H "Content-Type: application/json" -d '{`
    * `"name": "John Smith",`
    * `"email": "john@example.com"`
    * `}'`

4. **DELETE**:

    * `curl -X DELETE https://example.com/api/users/1`

5. **PATCH**:

    * `curl -X PATCH https://example.com/api/users/1 -H "Content-Type: application/json" -d '{`
    * `"name": "John Smith"`
    * `}'`

6. **OPTIONS**:

    * `curl -X OPTIONS https://example.com/api/users`

7. **HEAD**:

    * `curl -X HEAD https://example.com/api/users/1`

In each CURL example, -X specifies the HTTP method, -H sets the request header, and -d is used to send data in the request body (if required).
