# [Sql](https://trello.com/c/IxpFveow/237-1-what-is-sql-and-what-are-its-key-components)

## 1. What is SQL and what are its key components?

SQL (Structured Query Language) is a standard programming language used for managing relational databases. It provides a set of commands and syntax to interact with and manipulate data stored in a relational database management system (RDBMS). SQL is widely used across various database platforms, including PostgreSQL, MySQL, Oracle, SQL Server, and more.

The key components of SQL include:

1. **Data Definition Language (DDL)**: DDL statements are used to define and manage the structure of a database. They include commands such as CREATE, ALTER, and DROP to create tables, modify their structure, and delete them.

2. **Data Manipulation Language (DML)**: DML statements are used to manipulate data within the database. Common DML commands include SELECT, INSERT, UPDATE, and DELETE. These statements allow you to retrieve, insert, update, and delete data records from tables.

3. **Data Control Language (DCL)**: DCL statements are used to manage user access and permissions within the database. Commands like GRANT and REVOKE are used to grant or revoke privileges, roles, and permissions to users or user groups.

4. **Transaction Control Language (TCL)**: TCL statements are used to manage transactions in the database. Commands like COMMIT, ROLLBACK, and SAVEPOINT are used to ensure the integrity and consistency of data by controlling the transactional behavior.

5. **Query Language**: SQL is primarily a query language, which allows you to retrieve specific data from one or multiple tables using the SELECT statement. It includes various clauses like WHERE, GROUP BY, HAVING, and ORDER BY for filtering, grouping, and sorting data.

6. **Constraints**: Constraints are rules applied to tables to enforce data integrity. Common constraints include primary key, foreign key, unique, not null, and check constraints.

7. **Indexes**: Indexes are structures that improve the performance of database operations by allowing faster data retrieval. They are created on specific columns of a table to speed up searching, sorting, and joining operations.

## 2. What is the difference between SQL and PostgreSQL (psql)?

**SQL**: SQL is a standardized language used for managing and manipulating data stored in relational databases. It is a language that follows a specific syntax and set of commands to perform operations such as querying, inserting, updating, and deleting data in a structured manner. SQL is not tied to any specific database system but rather serves as a common language understood by various database platforms like PostgreSQL, MySQL, Oracle, SQL Server, and others.

**PostgreSQL (psql)**: PostgreSQL, often referred to as simply "Postgres," is an open-source object-relational database management system (RDBMS). It is one of the most popular and feature-rich database systems available. PostgreSQL fully supports SQL and implements the SQL standard, allowing users to interact with the database using SQL commands. Psql, on the other hand, is a command-line interface (CLI) provided by PostgreSQL that allows users to execute SQL commands, manage databases, and perform administrative tasks interactively.

(In summary, SQL is a language used for working with relational databases, while PostgreSQL (psql) is a specific implementation of an RDBMS that fully supports SQL and provides additional features and functionality beyond the SQL standard. Psql is the command-line interface provided by PostgreSQL to interact with the database using SQL commands and perform administrative tasks.)

## 3. What are the different types of SQL statements?

The main types of SQL statements are:

1. **Data Definition Language (DDL) Statements**:
        CREATE: Creates a new database, table, view, index, or other database objects.
        ALTER: Modifies the structure of an existing database object, such as a table or column.
        DROP: Deletes an existing database object, such as a table or view.
        TRUNCATE: Removes all data from a table while keeping the structure intact.

2. **Data Manipulation Language (DML) Statements**:
        SELECT: Retrieves data from one or more tables.
        INSERT: Inserts new records into a table.
        UPDATE: Modifies existing records in a table.
        DELETE: Removes records from a table.
        MERGE: Performs an "upsert" operation, combining INSERT and UPDATE based on specified conditions.

3. **Data Control Language (DCL) Statements**:
        GRANT: Grants specific privileges or permissions to database users.
        REVOKE: Revokes privileges or permissions previously granted to users.
        DENY: Denies certain actions or access to a user.

4. **Transaction Control Language (TCL) Statements**:
        COMMIT: Saves changes made within a transaction and makes them permanent.
        ROLLBACK: Discards changes made within a transaction and restores the previous state.
        SAVEPOINT: Sets a named point within a transaction, allowing partial rollbacks to specific savepoints.

5. **Data Query Language (DQL) Statements**:
        SELECT: Retrieves data from one or more tables based on specified conditions.
        JOIN: Combines records from two or more tables based on related columns.
        GROUP BY: Groups rows based on specified columns.
        HAVING: Filters grouped rows based on specified conditions.
        ORDER BY: Sorts the result set based on specified columns.

## 4. How do you select data from a table in SQL?

1. Specify the columns you want to retrieve in the SELECT clause. You can either specify specific column names or use an asterisk (*) to select all columns.

2. Specify the table from which you want to retrieve the data using the FROM clause.

    * **Example 1** (General):

      * `SELECT column1, column2, ... FROM table_name;`

3. Optionally, you can use additional clauses to filter, sort, or limit the result set. For example:

    * WHERE clause: Allows you to specify conditions to filter the rows based on certain criteria.

    * ORDER BY clause: Sorts the result set based on one or more columns in ascending (ASC) or descending (DESC) order.

    * LIMIT clause: Specifies the maximum number of rows to return from the result set.

**Example 2** (Select all columns from table "employees"):

* `SELECT * FROM employees;`

**Example 3** (Select specific columns, for example by name):

* `SELECT employee_id, first_name, last_name FROM employees;`

**Example 4** (Adding conditions and filtering the result set, for example with WHERE clause):

* `SELECT employee_id, first_name, last_name FROM employees WHERE job_title = 'Manager';`

## 5. What is the difference between the WHERE and HAVING clauses in SQL?

The WHERE and HAVING clauses are both used to filter data in SQL queries, but they are used in different contexts:

* **WHERE Clause**: used in a SELECT, UPDATE, or DELETE statement to filter rows based on specified conditions. It operates on individual rows before any grouping or aggregation takes place. It filters the rows that meet the specified conditions and includes only those rows in the result set.

  * `SELECT employee_id, first_name, last_name FROM employees WHERE job_title = 'Manager';`

* **HAVING Clause**: used in a SELECT statement with GROUP BY to filter the result set based on aggregated values. It operates on groups of rows after the data has been grouped using GROUP BY. It filters the groups that meet the specified conditions and includes only those groups in the result set.

  * `SELECT COUNT(CustomerID), Country FROM Customers GROUP BY Country HAVING COUNT(CustomerID) > 5;`

Key differences between WHERE and HAVING clauses:

1. **Usage**:

    * WHERE: Used with individual rows to filter data before grouping or aggregation.

    * HAVING: Used with grouped rows to filter aggregated data after grouping.

2. **Placement**:

    * WHERE: Appears before the GROUP BY clause (if present) in a SELECT statement.

    * HAVING: Appears after the GROUP BY clause in a SELECT statement.

3. **Aggregation**:

    * WHERE: Cannot use aggregate functions like SUM, COUNT, AVG, etc.

    * HAVING: Can use aggregate functions to define conditions based on the result of aggregations.

4. **Filtering Criteria**:

    * WHERE: Operates on individual rows and filters based on column values.

    * HAVING: Operates on groups of rows and filters based on aggregated values.

## 6. How do you sort data in SQL?

The ORDER BY clause allows you to specify one or more columns by which you want to sort the result set. The sorting can be done in ascending (ASC) or descending (DESC) order.

**Example 1**:

* `SELECT column1, column2, ... FROM table_name ORDER BY column1 ASC, column2 DESC;`

Step-by-step breakdown of how to sort data:

1. Specify the columns you want to select in the SELECT clause.

2. Specify the table from which you want to retrieve the data in the FROM clause.

3. Use the ORDER BY clause to specify the column(s) by which you want to sort the data. You can specify multiple columns separated by commas.

4. Optionally, specify the sorting order for each column. By default, the sorting order is ascending (ASC), but you can explicitly specify ascending or descending order using ASC or DESC, respectively.

**Example 2**:

* `SELECT column1, column2, ... FROM table_name ORDER BY column1 ASC;`

The ORDER BY clause can be used with various data types, including numeric, character, and date/time columns. It allows you to control the order in which the data is presented in the result set, making it easier to analyze and interpret the data.

## 7. What is the difference between INNER JOIN and OUTER JOIN in SQL?

In SQL, JOIN operations are used to combine rows from two or more tables based on a related column between them. The main types of JOIN operations are INNER JOIN and OUTER JOIN, each serving a different purpose:

1. INNER JOIN: An INNER JOIN returns only the rows that have matching values in both tables being joined. It combines rows from two tables where the join condition is satisfied, excluding any unmatched rows.

    * `SELECT table1.column1, table2.column2, ... FROM table1 INNER JOIN table2 ON table1.common_column = table2.common_column;`

    * In this example, the INNER JOIN combines rows from table1 and table2 where the values in the common_column match.

2. OUTER JOIN:
An OUTER JOIN returns all the rows from one table and the matching rows from the second table. If there is no match, NULL values are included for the columns of the table without a match. It allows you to retrieve unmatched rows as well.

    * There are three types of OUTER JOINs:

        1. LEFT OUTER JOIN (or simply LEFT JOIN): Returns all the rows from the left table and the matching rows from the right table. If there is no match in the right table, NULL values are included.

            * `SELECT table1.column1, table2.column2, ... FROM table1 LEFT JOIN table2 ON table1.common_column = table2.common_column;`

        2. RIGHT OUTER JOIN (or simply RIGHT JOIN): Returns all the rows from the right table and the matching rows from the left table. If there is no match in the left table, NULL values are included.

        3. FULL OUTER JOIN (or simply FULL JOIN): Returns all the rows from both tables and includes NULL values for unmatched rows in either table.

The choice between INNER JOIN and OUTER JOIN depends on the specific requirements of your query. If you only want matching rows, use INNER JOIN. If you want to include unmatched rows from one or both tables, use an appropriate type of OUTER JOIN (LEFT, RIGHT, or FULL).

## 8. How do you perform data manipulation (INSERT, UPDATE, DELETE) in SQL?

1. INSERT: The INSERT statement is used to add new records to a table. It allows you to specify the values for each column in the table or insert data from another table or query result.

    * `INSERT INTO table_name (column1, column2, ...) VALUES (value1, value2, ...);`

    * `INSERT INTO table_name (column1, column2, ...) SELECT value1, value2, ... FROM other_table WHERE condition;`

    * `INSERT INTO employees (first_name, last_name, job_title, salary) VALUES ('John', 'Doe', 'Manager', 5000);`

2. UPDATE: The UPDATE statement is used to modify existing records in a table. It allows you to change the values of specific columns in one or more rows based on specified conditions.

    * `UPDATE table_name SET column1 = value1, column2 = value2, ... WHERE condition;`

    * `UPDATE employees SET job_title = 'Senior Manager' WHERE employee_id = 123;`

3. DELETE: The DELETE statement is used to remove one or more records from a table. It allows you to specify conditions to identify the rows to be deleted.

    * `DELETE FROM table_name WHERE condition;`

    * `DELETE FROM employees WHERE employee_id = 123;`

## 9. What are SQL functions and how do you use them?

SQL functions are built-in operations that perform specific tasks or calculations on data. They accept one or more input values and return a single value or a result set. SQL functions are categorized into different types, including scalar functions, aggregate functions, and window functions, each serving a specific purpose.

1. **Scalar Functions**: Scalar functions operate on a single row of data and return a single value. They can be used in SELECT, WHERE, and other clauses to manipulate data or perform calculations.

    * `SELECT UPPER(column_name) FROM table_name;`

    * This query uses the UPPER function to convert the values in the column_name to uppercase.

2. **Aggregate Functions**: Aggregate functions perform calculations on a set of values and return a single result. They are used with the GROUP BY clause to summarize data across multiple rows.

    * `SELECT SUM(column_name) FROM table_name GROUP BY grouping_column;`

3. **Window Functions**: Window functions operate on a set of rows, called a "window," within the result set. They perform calculations based on the values in the window and return a result for each row.

    * `SELECT column1, column2, ..., ROW_NUMBER() OVER (ORDER BY column1) as row_num FROM table_name;`

SQL functions can also include mathematical functions (e.g., ABS, ROUND), string functions (e.g., CONCAT, SUBSTRING), date functions (e.g., DATE, DATEPART), and more. The specific functions available may vary depending on the database system you are using.
You can combine functions, nest them within each other, and use them in conjunction with other SQL statements to achieve desired data manipulations and calculations.

## 10. How do you group data using the GROUP BY clause in SQL?

The GROUP BY clause in SQL is used to group rows based on one or more columns and perform aggregate functions on each group. It allows you to summarize data and retrieve results at a higher level of granularity.

Step-by-step breakdown of how to use the GROUP BY clause:

1. Specify the columns you want to select in the SELECT clause. These can include columns to be grouped by and/or columns on which you want to perform aggregate functions.

2. Specify the table from which you want to retrieve the data in the FROM clause.

3. Use the GROUP BY clause to specify the column(s) by which you want to group the data. You can specify multiple columns separated by commas.

4. Optionally, use one or more aggregate functions in the SELECT clause to perform calculations on each group. Aggregate functions include COUNT, SUM, AVG, MIN, MAX, etc.

**Example 1**:

* `SELECT column1, COUNT(column2), SUM(column3) FROM table_name GROUP BY column1;`

  * In this example, the data is grouped by column1. The query calculates the count of column2 and the sum of column3 for each group.

**Example 2**:

* `SELECT product, SUM(quantity) AS total_quantity, AVG(price) AS average_price FROM sales_table GROUP BY product;`

  * In this example, the data is grouped by the "product" column. The SUM function calculates the total quantity sold for each product, while the AVG function calculates the average price per product. The result is a table that displays the grouped data along with the corresponding total quantity and average price for each distinct product.

## 11. What is a subquery and how do you use it in SQL?

A subquery, also known as a nested query or inner query, is a query nested within another query in SQL. It is a powerful feature that allows you to use the results of one query as input for another query. The result of a subquery can be used in various ways, such as filtering data, performing calculations, or retrieving additional information.

**Example**:

* `SELECT column1, column2 FROM table1 WHERE column3 IN (SELECT column4 FROM table2 WHERE condition);`

Subqueries can be used in different parts of a SQL statement, such as the SELECT, FROM, WHERE, HAVING, and JOIN clauses.

## 12. What is the difference between UNION and UNION ALL in SQL?

Both are used to combine the result sets of two or more SELECT statements into a single result set. However, there is a key difference between them:

1. **UNION**: The UNION operator combines the result sets of multiple SELECT statements, eliminating duplicate rows from the final result set.

2. **UNION ALL**: The UNION ALL operator also combines the result sets of multiple SELECT statements but does not eliminate duplicate rows. It includes all rows from each SELECT statement, regardless of duplicates.

**Example**:

* `SELECT Column1 FROM Table1 UNION SELECT Column1 FROM Table2;`

## 13. How do you handle NULL values in SQL?

1. **IS NULL**: You can use the IS NULL operator to check if a column has a NULL value.

    * `SELECT column1 FROM table_name WHERE column1 IS NULL;`

2. **IS NOT NULL**: Conversely, the IS NOT NULL operator is used to check if a column has a non-NULL value.

    * `SELECT column1 FROM table_name WHERE column1 IS NOT NULL;`

3. **COALESCE**: The COALESCE function is used to return the first non-NULL expression in a list. It can be helpful in replacing NULL values with a specific value.

    * `SELECT COALESCE(column1, 'N/A') FROM table_name;`

      * This query will return the value of column1 if it is not NULL. Otherwise, it will return 'N/A'.

4. **NULLIF**: The NULLIF function compares two expressions and returns NULL if they are equal. It can be useful for handling cases where you want to treat specific values as NULL.

    * `SELECT NULLIF(column1, 0) FROM table_name`

      * This query will return NULL if the value of column1 is 0.

5. **IFNULL/ISNULL**: Some database systems provide the IFNULL or ISNULL functions, which return a specified value if the expression is NULL.

    * `SELECT IFNULL(column1, 'N/A') FROM table_name;` -- IFNULL in MySQL

    * `SELECT ISNULL(column1, 'N/A') FROM table_name;` -- ISNULL in SQL Server

      * These queries will return the value of column1 if it is not NULL. Otherwise, they will return 'N/A'.

6. **Aggregate Functions**: When using aggregate functions like SUM, AVG, COUNT, etc., NULL values are often automatically ignored. The aggregate functions will only operate on non-NULL values in the specified column(s).

## 14. What is a transaction and how do you manage it in SQL?

[EXAMPLES](https://trello.com/c/AniiJgFO/256-14b-transaction-examples)

A transaction is a logical unit of work that consists of one or more database operations. These operations can be data manipulation statements (such as INSERT, UPDATE, DELETE) or data definition statements (such as CREATE, ALTER, DROP). The purpose of a transaction is to ensure data integrity and consistency by treating a group of operations as a single, indivisible unit.

The ACID properties define the characteristics of a transaction:

1. **Atomicity**: A transaction is atomic, meaning that it is treated as a single unit of work. Either all the operations within the transaction are executed successfully, or none of them are. If any operation fails, the entire transaction is rolled back, and the database is left unchanged.

2. **Consistency**: A transaction brings the database from one consistent state to another. It ensures that data modifications follow predefined rules and constraints, maintaining data integrity.

3. **Isolation**: Each transaction is isolated from other transactions executing concurrently. Intermediate results of a transaction are not visible to other transactions until the transaction is committed. This prevents interference and maintains data integrity.

4. **Durability**: Once a transaction is committed, its changes are permanently stored and survive system failures. The changes become a permanent part of the database and cannot be undone.

To manage transactions in SQL, you typically use transaction control statements:

1. **BEGIN TRANSACTION (or BEGIN WORK)**: This statement marks the start of a transaction. It defines the boundary of the transaction and sets the transaction isolation level.

2. **COMMIT**: The COMMIT statement is used to save the changes made within a transaction. It makes the changes permanent and ends the transaction.

3. **ROLLBACK**: The ROLLBACK statement is used to undo the changes made within a transaction and return the database to its previous state. It is typically used when an error occurs or when the transaction needs to be canceled.

4. **SAVEPOINT**: SAVEPOINT allows you to set a point within a transaction to which you can later roll back. It provides a way to create checkpoints within a transaction and selectively rollback to a specific point if needed.

## 15. What are SQL indexes and how do they improve performance?

SQL indexes are database structures that improve the performance of queries by enabling fast data retrieval. They are created on one or more columns of a table and store a sorted copy of the data values, along with a pointer to the corresponding table rows. When a query is executed, the database engine can use the index to quickly locate the desired data, reducing the need for full table scans and improving query response time.

1. **Fast Data Retrieval**: Indexes provide a way to access specific data quickly. By storing a sorted copy of the data values, the database engine can perform an index lookup to locate the relevant rows, bypassing the need to scan the entire table. This leads to faster query execution times, especially when dealing with large tables.

2. **Reduced Disk I/O**: Indexes can reduce the amount of disk I/O required for query execution. Instead of reading the entire table, the database engine can fetch the required data pages directly from the index, resulting in fewer disk reads and improved performance.

3. **Query Optimization**: The database optimizer can utilize indexes to optimize query execution plans. By analyzing the available indexes, the optimizer can choose the most efficient access path to retrieve the data, such as performing index scans, index seeks, or index joins. This leads to better query performance and reduced resource consumption.

4. **Sorting and Ordering**: Indexes are inherently sorted structures. This allows the database engine to quickly satisfy sorting and ordering requirements of queries without the need for additional sorting operations. For example, when an ORDER BY clause is used, the engine can directly read the data from the index in the desired order, avoiding the need to sort the entire result set.

5. **Constraint Enforcement**: In addition to improving query performance, indexes can enforce data integrity constraints such as uniqueness and referential integrity. Unique indexes ensure that no duplicate values exist in the indexed column(s), while foreign key indexes enforce the relationship between tables. These indexes speed up constraint validation and prevent inconsistent data.

Indexes come with some trade-offs. They require additional storage space and can slow down data modification operations (such as INSERT, UPDATE, DELETE) because indexes need to be updated along with the data. Therefore, careful consideration should be given to the selection and maintenance of indexes to strike a balance between improved query performance and efficient data modification.

## 16. How do you create and manage views in SQL?

View is a virtual table that is derived from the result of a query. It provides a way to encapsulate complex queries or frequently used queries into a reusable object. Views can be used to simplify data access, enhance security, and improve query performance.

1. **Creating a View**: To create a view, you use the CREATE VIEW statement followed by a query that defines the view's data.

    * `CREATE VIEW view_name AS SELECT column1, column2, ... FROM table_name WHERE condition;`

    * `CREATE VIEW customer_emails AS SELECT name, email FROM customers;`

        * View that retrieves the names and emails of customers from a customers table.

2. **Accessing a View**: Once a view is created, it can be accessed using the SELECT statement.

    * `SELECT * FROM view_name;`

    * `SELECT * FROM customer_emails;`

        * This query will retrieve the data from the customer_emails view.

3. **Updating a View**: Depending on the database system and the view definition, you may be able to update the underlying tables through a view. The ability to update views depends on factors such as the complexity of the view's query, the presence of certain join types, and the presence of certain aggregate functions. However, some views are read-only, and updates through the view are not allowed.
If updates are allowed, you can use the UPDATE, INSERT, or DELETE statements on the view to modify the underlying data.

4. **Modifying a View**: If you want to modify the definition of an existing view, you can use the ALTER VIEW statement. The syntax varies depending on the database system.

    * `ALTER VIEW view_name AS SELECT new_columns FROM new_tables WHERE new_condition;`

        * This statement allows you to change the columns, tables, or conditions used in the view.

5. **Dropping a View**: If you no longer need a view, you can remove it from the database using the DROP VIEW statement.

    * `DROP VIEW view_name;`

        * This will delete the view view_name.

## 17. What are triggers and how do you use them in SQL?

A trigger is a database object that is associated with a table and automatically executes a set of actions (code) when a specific event occurs, such as an insert, update, or delete operation on the table.

1. **Creating a Trigger**: To create a trigger, you use the CREATE TRIGGER statement, specifying the trigger name, the table it is associated with, and the event that triggers it (INSERT, UPDATE, or DELETE). You also define the actions to be performed in response to the event.

    * `CREATE TRIGGER trigger_name AFTER INSERT OR UPDATE OR DELETE ON table_name FOR EACH ROW BEGIN -- Trigger actions here END;`

        * You can define multiple trigger actions within the BEGIN and END block.

2. **Referencing the Trigger Event**: Inside the trigger, you can reference the new or updated values of the affected rows using the NEW keyword. For example, NEW.column_name represents the new value of a column in an INSERT or UPDATE trigger.
If you need to reference the old values of the affected rows in an UPDATE or DELETE trigger, you can use the OLD keyword. For example, OLD.column_name represents the old value of a column in an UPDATE or DELETE trigger.

3. **Trigger Actions**:
Trigger actions are the set of SQL statements or procedures that are executed when the trigger event occurs. The actions can include data modifications, data validation, logging, or calling stored procedures.

    * [Example](https://trello.com/c/xY5BRA71/253-17-what-are-triggers-and-how-do-you-use-them-in-sql)

4. **Managing Triggers**: Triggers can be altered, enabled, disabled, or dropped using appropriate SQL statements. The specific syntax for managing triggers varies between database systems.

    * `ALTER TRIGGER trigger_name DISABLE;`

        * To disable a trigger.

    * `DROP TRIGGER trigger_name;`

        * To drop a trigger

    * Refer to the documentation of your specific database system for the exact syntax and options available for managing triggers.

## 18. How do you handle database security and user permissions in SQL?

[Examples](https://trello.com/c/C6VvKnxp/254-18-how-do-you-handle-database-security-and-user-permissions-in-sql)

Database security and user permissions in SQL are managed through authentication, authorization, and user management.

1. **Authentication**: Verify user identities using usernames and passwords.

2. **Authorization**: Grant specific permissions (e.g., SELECT, INSERT, UPDATE, DELETE) to users or roles.

3. **Roles and RBAC**: Assign permissions to roles and assign roles to users to simplify access control.

4. **Granting Permissions**: Use the GRANT statement to grant permissions on database objects (e.g., tables, views) to users or roles.

5. **Revoking Permissions**: Use the REVOKE statement to revoke permissions from users or roles.

6. **Views**: Restrict access to sensitive data by granting permissions on views instead of underlying tables.

7. **Regular Audits**: Perform security audits to identify vulnerabilities, review user permissions, and ensure compliance with security policies.

Implementing strong passwords, limiting privileges, patching software, and implementing network security measures are also crucial for database security.

## 19. What are stored procedures and how do you use them in SQL?

Stored procedures are precompiled and stored database objects that encapsulate a set of SQL statements. They allow you to group multiple SQL statements into a single unit, which can be invoked and executed repeatedly.

1. **Creating a Stored Procedure**: To create a stored procedure, you use the CREATE PROCEDURE statement. Inside the procedure, you define the SQL statements that make up its functionality.

    * `CREATE PROCEDURE procedure_name ...` - rest on trello

2. **Executing a Stored Procedure**: To execute a stored procedure, you use the CALL statement or EXECUTE / EXEC statement, followed by the procedure name and any required parameter values.

    * `CALL procedure_name;`

    * `EXECUTE procedure_name;`

    * `CALL procedure_name(parameter1_value, parameter2_value);`

      * If the procedure has parameters, you provide their values within the parentheses.

3. **Modifying a Stored Procedure**:
Stored procedures can be altered or updated using the ALTER PROCEDURE statement. You can modify the SQL statements within the procedure body or change the parameter list.

    * `ALTER PROCEDURE procedure_name [parameter1 datatype1, parameter2 datatype2, ...]` - rest on trello

4. **Dropping a Stored Procedure**:
To remove a stored procedure, you use the DROP PROCEDURE statement, followed by the procedure name.

    * `DROP PROCEDURE procedure_name;`

Stored procedures offer several benefits, including:

* Code Reusability: Once defined, a stored procedure can be invoked multiple times, reducing the need to write the same SQL statements repeatedly.

* Improved Performance: Stored procedures are precompiled and stored in the database, allowing for faster execution and reduced network traffic.

* Enhanced Security: Users can be granted permissions to execute specific stored procedures without granting direct table-level access.

* Business Logic Encapsulation: Complex business logic can be encapsulated within a stored procedure, providing a centralized and controlled way to perform operations on data.

## 20. How do you optimize SQL queries for better performance?

1. **Use Indexes**: Indexes help speed up data retrieval by creating a searchable structure. Identify columns frequently used in search conditions or join predicates and create indexes on them. However, be cautious with over-indexing, as it can impact insert and update performance.

2. **Write Efficient Queries**: Construct queries that retrieve only the necessary data. Minimize the use of wildcards (*) and retrieve only the required columns. Use appropriate filtering conditions in the WHERE clause to limit the result set.

3. **Avoid Subqueries**: Subqueries can be performance-intensive. Whenever possible, try to rewrite subqueries as joins to leverage the database's optimized join algorithms.

4. **Optimize Joins**: Use appropriate join types (e.g., INNER JOIN, LEFT JOIN) based on the relationship between tables. Ensure that join conditions are indexed. Consider using JOIN hints or rearranging the order of joined tables to improve performance.

5. **Avoid N+1 Problem**: When retrieving related data using a loop, make use of JOINs or batch retrieval techniques like the IN clause or EXISTS subquery to fetch all required data in a single query instead of separate queries for each loop iteration.

6. **Limit Result Set**: If you only need a specific number of rows, use the LIMIT clause to restrict the result set. This reduces the amount of data transferred and improves query performance.

7. **Normalize Database Schema**: Normalize the database schema to eliminate redundancy and improve query efficiency. Normalize tables to reduce the need for complex joins and improve data integrity.

8. **Update Statistics**: Regularly update statistics on tables and indexes to help the query optimizer make better decisions. Outdated statistics can lead to inefficient query plans.

9. **Consider Caching**: Utilize caching mechanisms at the application level to store and reuse frequently accessed data, reducing the need for repetitive queries.

10. **Monitor Query Performance**: Use database monitoring tools to analyze query performance, identify slow-running queries, and optimize them by adding appropriate indexes, rewriting queries, or restructuring the data model if necessary.

## 21. What are database dumps?

A database dump refers to the process of creating a backup or snapshot of a database's contents at a specific point in time. It involves extracting and saving the data from a database into a file or set of files.

The resulting files usually contain the structure of the database schema and the data stored within it, allowing for the recreation of the database or the restoration of its contents if necessary.

Key purposes:

1. **Backup and recovery**: Database dumps are commonly used as a backup mechanism to safeguard against data loss or corruption. By regularly creating database dumps, organizations can restore the database to a previous state in case of hardware failures, software bugs, or human errors.

2. **Data migration**: Database dumps facilitate the transfer of data from one database system to another. By creating a dump from the source database and importing it into the destination database, the data can be efficiently migrated, ensuring data integrity and consistency.

3. **Development and testing**: Database dumps are valuable for developers and testers to work with realistic data sets that mimic the production environment. By restoring a database dump into a development or testing environment, they can perform experiments, test code changes, and analyze data without affecting the live system.

4. **Analysis and reporting**: Database dumps provide a static snapshot of data that can be used for analysis, reporting, and generating insights. Researchers, data scientists, and analysts can examine the data using various tools and techniques, without worrying about real-time updates or modifications to the original database.

## Database Backup and Restore

[Backup and Restore process](https://trello.com/c/egyl4kTU/262-database-backup-and-restore-dump)
