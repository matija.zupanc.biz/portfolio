
N = int(input('''Enter the positive integer to calculate the highest power of 2
that divides it:
>'''))


def solution(N):
    divider = 1
    max = 0

    while 2**divider <= N:
        if N % 2**divider == 0:
            max = divider
        divider += 1

    return print(max)


solution(N)
