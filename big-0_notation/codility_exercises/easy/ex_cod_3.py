# If a string is palindrom find the length of the characters on one side
# of it ex: 'racecar' = 3

from math import ceil


S = input('''Enter the string
>''')


def solution(S):
    if len(S) % 2 == 0:
        return print(-1)
    else:
        index = ceil(len(S) / 2)
        left_side = S[:index]
        right_side = S[index-1:]
        reverse_right = right_side[::-1]

        if left_side == reverse_right:
            return print(len(left_side) - 1)
        else:
            return print(-1)


solution(S)
