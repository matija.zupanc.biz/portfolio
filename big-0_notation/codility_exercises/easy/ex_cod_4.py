

A = [4, 10, 5, 4, 2, 10]
A1 = [1, 4, 3, 3, 1, 2]
A2 = [6, 4, 4, 6]


# def solution(A):
#     all = []
#     double = []

#     for num in A:
#         if num not in all:
#             all.append(num)
#         else:
#             double.append(num)
#     for item in double:
#         all.remove(item)

#     if len(all) == 0:
#         return print(-1)
#     else:
#         return print(all[0])


def solution(A):
    occurrences = {}  # Dictionary to count occurrences of each number
    purged_list = []  # List to maintain the order of appearance

    # Count occurrences and maintain order
    for num in A:
        if num in occurrences:
            occurrences[num] += 1
        else:
            occurrences[num] = 1
            purged_list.append(num)

    # Find the first unique number
    for num in purged_list:
        if occurrences[num] == 1:
            return print(num)

    return print(-1)  # No unique number found


solution(A)
