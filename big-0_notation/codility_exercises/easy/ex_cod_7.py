

N = int(input('''Enter a positive integer:
>'''))


def solution(N):
    bin_N = bin(N)[2:]
    characters = [*bin_N]
    list_N = bin_N.split('1')
    if characters[0] == '0':
        list_N.pop(0)
    if characters[-1] == '0':
        list_N.pop(-1)
    max = 0
    for item in list_N:
        if len(item) > max:
            max = len(item)
    return print(max)


solution(N)
