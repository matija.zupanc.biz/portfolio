# Find longest password in the string in regards to these rules:
# - has to contain only alphanumerical characters (a−z, A−Z, 0−9)
# - should be an even number of letters
# - should be an odd number of digits

S = input(
    '''According to the following restrictions:
- has to contain only alphanumerical characters (a−z, A−Z, 0−9)
- should be an even number of letters
- should be an odd number of digits
write a list of passwords separated by space:
> '''
)


# def solution(S):
#     password_list = S.split(' ')
#     checklist = ['false', 'false', 'false']
#     max_pass = -1

#     for password in password_list:
#         if password.isalnum() is True:
#             checklist[0] = 'true'
#             num_of_letters = 0
#             num_of_numbers = 0
#             for char in password:
#                 if char.isalpha():
#                     num_of_letters += 1
#                 if char.isdigit():
#                     num_of_numbers += 1
#             if num_of_letters % 2 == 0:
#                 checklist[1] = 'true'
#             if num_of_numbers % 2 != 0:
#                 import pdb; pdb.set_trace()
#                 checklist[2] = 'true'
#             if checklist == ['true', 'true', 'true']:
#                 if max_pass < len(password):
#                     max_pass = len(password)
#                     the_pass = password

#     return print(max_pass, the_pass)


def solution(S):
    password_list = S.split()
    max_length = -1

    for password in password_list:
        num_letters = sum(1 for char in password if char.isalpha())
        num_digits = sum(1 for char in password if char.isdigit())

        if password.isalnum() and num_letters % 2 == 0 and num_digits % 2 != 0:
            max_length = max(max_length, len(password))

    return print(max_length)


solution(S)
