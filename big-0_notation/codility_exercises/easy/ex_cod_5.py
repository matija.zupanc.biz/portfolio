import re
from math import ceil


E = "08:30"
L = "10:35"


def solution(E, L):
    entrance_fee = 2
    first_hour = 3
    other_hour = 4

    # Remove ":" from strings so that they can be easily used in calculation
    E_cleaned = int(re.sub(r':', '', E))
    L_cleaned = int(re.sub(r':', '', L))

    time_parked = ceil((L_cleaned - E_cleaned) / 100)
    full_fee = entrance_fee + first_hour + (other_hour * (time_parked-1))

    return print(full_fee)


solution(E, L)
