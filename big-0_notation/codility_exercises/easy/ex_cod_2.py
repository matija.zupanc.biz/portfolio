# Find maximum number of courts that can be used at the same time

P = int(input(
    '''How many players will be playing:
> '''
))

C = int(input(
    '''How many courts have been reserved:
> '''
))


# def solution(P, C):
#     max_parallel_games = 0
#     pairs = P // 2
#     while C > 0 and pairs > 0:
#         max_parallel_games += 1
#         C -= 1
#         pairs -= 1
#     return print("Max parallel games played:", max_parallel_games)


def solution(P, C):
    max_parallel_games = min(P // 2, C)
    return max_parallel_games


solution(P, C)
