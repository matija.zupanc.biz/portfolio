from collections import Counter


# K = 2
# C = [1, 2, 1, 1]
# D = [1, 4, 3, 2, 4]

K = 3
C = [1, 2]
D = [8, 8, 8, 8, 9]


def solution(K, C, D):
    clean_count = Counter(C)
    dirty_count = Counter(D)
    max_pairs = 0

    for clean_sock in clean_count:
        if clean_count[clean_sock] > 1:
            clean_pairs = clean_count[clean_sock] // 2
            max_pairs += clean_pairs
            clean_count[clean_sock] -= (clean_pairs * 2)
        if clean_count[clean_sock] == 1 and clean_sock in D and K > 0:
            max_pairs += 1
            dirty_count[clean_sock] -= 1
            K -= 1

    if K >= 2:
        for dirty_sock in dirty_count:
            if dirty_count[dirty_sock] >= 2:
                dirty_pairs = dirty_count[dirty_sock] // 2
                while K >= 2 and dirty_pairs > 0:
                    K -= 2
                    dirty_pairs -= 1
                    max_pairs += 1

    return print(max_pairs)


solution(K, C, D)
