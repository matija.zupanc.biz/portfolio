from collections import Counter


A = [1, 2, 5, 1, 1, 2, 3, 5, 1]
X = 5


def solution(A, X):
    # Count the number of times an element is located in a list.
    fence_count = Counter(A)

    num_of_pens = 0
    usable_fences = []
    for fence in fence_count:
        if fence_count[fence] < 2:
            # Less than one pair. We cannot use it.
            continue
        elif fence_count[fence] < 4:
            usable_fences.append(fence)
        else:
            usable_fences.append(fence)
            # We consider the square pen here.
            # (A case when we have 4 or more of the same element).
            if fence * fence >= X:
                num_of_pens += 1
    # We consider the non-square pen here. ("Normal" cases).
    # We need to sort() the list in order to perform binary search
    usable_fences.sort()
    usable_size = len(usable_fences)
    for i in range(usable_size):
        # Use binary search to find the first fence pair, that
        # could be used with current pair to form a pen.
        begin = i + 1
        end = usable_size - 1
        while begin <= end:
            mid = (begin + end) // 2
            if usable_fences[mid] * usable_fences[i] >= X:
                # If the product is more or the same as "X" move the end point.
                end = mid - 1
            else:
                # If the product is less than "X" move the begin point.
                begin = mid + 1
        # Now the usable_fences[end + 1] is the first qualified
        # fence. So the equation calculates the difference between the total
        # number of elements and the index of the first qualified element
        combination_num = usable_size - (end + 1)
        # Increment variable by the number of combinations found in the current
        # iteration (for "i").
        num_of_pens += combination_num
        if num_of_pens > 1000000000:
            return -1
    return print(num_of_pens)


solution(A, X)
