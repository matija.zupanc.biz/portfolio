

A = 1
B = 4


# def solution(A, B):
#     string = ''
#     count_A = 0
#     count_B = 0

#     while A > 0 or B > 0:
#         import pdb; pdb.set_trace()
#         if A > B and B > 0:
#             string += 'b'
#             B -= 1
#             count_B += 1
#             count_A = 0
#         elif A > 0:
#             string += 'a'
#             A -= 1
#             count_A += 1
#             count_B = 0
#         else:
#             break

#     return print(string)


def solution(A, B):
    result = []

    while A > 0 or B > 0:
        # If there are 'a's left and it won't result in three consecutive 'a's
        if A > B and result[-2:] != ['a', 'a']:
            result.append('a')
            A -= 1
        # If there are 'b's left and it won't result in three consecutive 'b's
        elif B >= A and result[-2:] != ['b', 'b']:
            result.append('b')
            B -= 1
        # If appending 'a' would result in three consecutive 'b's, or vice
        # versa, append the other letter
        else:
            if result[-1] == 'a':
                result.append('b')
                B -= 1
            else:
                result.append('a')
                A -= 1

    return print(''.join(result))


solution(A, B)
