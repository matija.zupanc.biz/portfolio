

A = [-1, 6, 3, 4, 7, 4]


# def solution(A):
#     count_inversions = 0
#     inversions = []
#     for index, number in enumerate(A):
#         i = index + 1
#         # import pdb; pdb.set_trace()
#         while i <= len(A) - 1:
#             if number > A[i]:
#                 count_inversions += 1
#                 inversions.append((index, i))
#             i += 1
#     print(inversions)
#     if count_inversions > 1000000000:
#         return print(-1)
#     else:
#         return print(count_inversions)


def solution(A):
    def merge(left, right):
        inversions = 0
        i = j = 0
        merged = []

        while i < len(left) and j < len(right):
            if left[i] <= right[j]:
                merged.append(left[i])
                i += 1
            else:
                merged.append(right[j])
                inversions += len(left) - i
                j += 1

        merged.extend(left[i:])
        merged.extend(right[j:])

        return merged, inversions

    def merge_sort(arr):
        if len(arr) <= 1:
            return arr, 0

        mid = len(arr) // 2
        left, left_inversions = merge_sort(arr[:mid])
        right, right_inversions = merge_sort(arr[mid:])
        merged, merge_inversions = merge(left, right)

        return merged, left_inversions + right_inversions + merge_inversions

    sorted_array, inversions = merge_sort(A)

    if inversions > 1000000000:
        return print(-1)
    else:
        return print(inversions)


solution(A)
