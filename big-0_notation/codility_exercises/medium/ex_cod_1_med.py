

A = [1, 3, 2, 1, 2, 1, 5, 3, 3, 4, 2]


# def solution(A):
#     max_depth = 0
#     full_depth = 0
#     wall_size = 0
#     low_point = 0
#     potential_depth = 0
#     previous_item = None

#     for index, item in enumerate(A):
#         if previous_item is not None:
#             if item < previous_item and wall_size == 0:
#                 wall_size = previous_item
#                 low_point = item
#             if item < low_point:
#                 low_point = item
#             if item >= wall_size and wall_size != 0:
#                 full_depth = wall_size - low_point
#                 max_depth = max(max_depth, full_depth)
#                 wall_size = 0
#                 low_point = 0
#                 full_depth = 0
#             if item < wall_size and item > low_point:
#                 potential_depth = item - low_point
#                 max_depth = max(max_depth, potential_depth)
#                 potential_depth = 0

#         previous_item = item

#     return print(max_depth)


def solution(A):
    n = len(A)
    if n < 3:
        return 0  # Not enough blocks to hold water

    left_max = [0] * n
    right_max = [0] * n

    # Calculate the maximum height to the left of each block
    left_max[0] = A[0]
    for i in range(1, n):
        left_max[i] = max(left_max[i - 1], A[i])

    # Calculate the maximum height to the right of each block
    right_max[n - 1] = A[n - 1]
    for i in range(n - 2, -1, -1):
        right_max[i] = max(right_max[i + 1], A[i])

    max_depth = 0

    # Calculate the depth of water that can be held at each block
    for i in range(1, n - 1):
        depth = min(left_max[i], right_max[i]) - A[i]
        max_depth = max(max_depth, depth)

    return print(max_depth)


solution(A)
