# Given a list of integers, find the pair of elements that sum to a specific
# target.


list_1 = [2, 4, 6, 8, 10, 10, 2]
list_2 = [1, -3, -11, 4, -5]
list_3 = [3, 1, 4, 1, 5, 9, 2, 6, 5]
list_4 = []
list_5 = [7]

target_1 = 12
target_3 = 1
target_4 = 0
target_5 = -16


# Doesn't take into account duplicates so it's not 100% correct always
def sum_of_pair(list_nums, target):
    if len(list_nums) < 2:
        return "List too small"

    num_set = set()
    result = []

    for num in list_nums:
        complement = target - num
        if complement in num_set:
            result.append([complement, num])
        num_set.add(num)

    return result


print(sum_of_pair(list_1, target_1))

# Time complexity: O(n)
