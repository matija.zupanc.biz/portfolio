# Determine if two lists are anagrams of each other.


list_1 = ['a', 'b', 'c', 'd', 'e']
list_2 = ['e', 'd', 'c', 'b', 'a']

list_3 = ['a', 'b', 'f', 'd', 'e']
list_4 = ['e', 'd', 'c', 'b', 'a', 'e']


def are_anagrams(list_1, list_2):
    if len(list_1) != len(list_2):
        return print(False)

    count = {}
    for item in list_1:
        count[item] = count.get(item, 0) + 1

    for item in list_2:
        if item not in count or count[item] == 0:
            return print(False)
        count[item] -= 1

    return print(True)


are_anagrams(list_3, list_4)

# Time complexity: O(n)
