# Check if a number is prime.

import random


number_0 = 23
number_1 = 34
number_2 = 2
number_3 = 1
number_4 = 0
number_5 = 7
number_6 = 8.0
number_7 = 6.4
number_8 = 17.3
number_9 = -7
number_10 = 1000001  # Not a prime


# Maybe better (chat gpt solution) - for large scale numbers (well above
# million)
def is_prime(number):
    if not isinstance(number, int) or number < 2:
        return False

    # Check small prime numbers
    if number in (2, 3):
        return True

    # Check divisibility by small primes
    if any(number % prime == 0 for prime in (2, 3, 5)):
        return False

    # Perform Miller-Rabin primality test
    def miller_rabin(n, k):
        if n < 2:
            return False

        # Write n-1 as (2^r) * d
        r, d = 0, n - 1
        while d % 2 == 0:
            r += 1
            d //= 2

        # Perform k iterations of the test
        for _ in range(k):
            a = random.randint(2, n - 2)
            x = pow(a, d, n)

            if x == 1 or x == n - 1:
                continue

            for _ in range(r - 1):
                x = pow(x, 2, n)
                if x == n - 1:
                    break
            else:
                return False

        return True

    # Perform 10 iterations of the Miller-Rabin test
    return miller_rabin(number, 10)


print(is_prime(1000001))

# Time complexity:  O(log^3(n))
