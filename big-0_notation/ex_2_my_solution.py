# Determine whether a given string is a palindrome.
# palindrome (a word, phrase, or sequence that reads the same backwards as
# forwards, e.g. madam or nurses run.)

import re


sequence_1 = 'madam'
sequence_2 = 'nurses run'
sequence_3 = 'pero'
sequence_4 = 'nabijem te jako'
sequence_5 = 123456789
sequence_6 = 3546453
sequence_7 = ['a', 'c', '3', '7', '24', 'z']
sequence_8 = ['a', '7', '24', 'z', '24', '7', 'a']
sequence_9 = ['a', 'c', 3, 7, 24, 'z']
sequence_10 = ['a', 7, 24, 'z', 24, 7, 'a']


def is_palindrome(sequence):
    if type(sequence) != list:
        sequence = list(re.sub(r"\s+", "", str(sequence)))

    reverse_sequence = sequence.copy()
    reverse_sequence.reverse()
    if sequence == reverse_sequence:
        print('List is a palindrom')
    else:
        print('Not a palindrom')

    return sequence


is_palindrome(sequence_10)

# Time complexity: O(n)
