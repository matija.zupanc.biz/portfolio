# Reverse a string.


example_string_1 = 'straight'
example_string_2 = 'reverse'
example_string_3 = 'madam'
example_string_4 = '17'
example_string_5 = 7263
example_string_6 = ''


def reverse_string(string):
    reverse_str = str(string)
    return reverse_str[::-1]


print(reverse_string(example_string_5))

# Time complexity: O(n)
