# Count the number of occurrences of elements in a list and return those that
# occurr multiple times and their count.

from collections import Counter


my_list_1 = [1, 2, 3, 4, 5]
my_list_2 = [5, 4, 3, 2, 1]
my_list_3 = [3, 1, 4, 1, 5, 9, 2, 6, 5]
my_list_4 = []
my_list_5 = [7]
my_list_6 = ['a', 'b', 'c', 'd', 'e']
my_list_7 = ['e', 'd', 'c', 'b', 'a']
my_list_8 = ['a', 'b', 'c', 'a', 'b', 'c', 'b']
my_list_9 = ['apple', 'banana', 'cherry', 'date', 'elderberry']
my_list_10 = [
    'elderberry', 'date', 'cherry', 'banana', 'cherry', 'apple', 'cherry']
my_list_11 = ['apple', 4, 'banana', 'apple', 'cherry', '4', 'elderberry', 4]


def find_elements_with_multiple_occurrences(my_list):
    count_dict = Counter(my_list)
    result = {
        element: count for element, count in count_dict.items() if count > 1
    }
    return result


print(find_elements_with_multiple_occurrences(my_list_11))

# Time complexity: O(n)
