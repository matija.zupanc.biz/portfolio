# Find the maximum element in a list.

numbers_int = [5, 2, 9, 1, 7, 3]
numbers_float = [3.14, 2.718, 1.618, 0.577, 2.303]
numbers_neg = [-10, 5, -3, 8, -2, 0]
numbers_str = ['10', '25', '7', '42', '13']


def find_max_el(start_list):
    if len(start_list) > 1:
        max_num = start_list[0]
        for num in start_list:
            if int(num) > int(max_num):
                max_num = int(num)
        return print(f'Max value is {max_num}')
    elif len(start_list) == 1 and start_list is not None:
        return print(f'List has only one value: {start_list[0]}')
    else:
        return print('List is empty')


find_max_el(numbers_int)

# Time complexity: O(n)
