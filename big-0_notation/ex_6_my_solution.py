# Determine if two lists are anagrams of each other.


list_1 = ['a', 'b', 'c', 'd', 'e']
list_2 = ['e', 'd', 'c', 'b', 'a']

list_3 = ['a', 'b', 'f', 'd', 'e']
list_4 = ['e', 'd', 'c', 'b', 'a', 'e']


def list_anagrams(list_1, list_2):
    if len(list_1) != len(list_2):
        return print("Two lists are not anagrams of each other")

    for item in list_1:
        if item not in list_2:
            return print("Two lists are not anagrams of each other")
    return print("Lists ARE anagrams")


list_anagrams(list_4, list_3)

# Time complexity: O(n^2)
