# Compute the Fibonacci sequence up to a given number.


given_num = 20


def compute_fibonacci(N):
    fibonacci_sequence = []
    n1 = 0
    n2 = 1

    for _ in range(N):
        fibonacci_sequence.append(n1)
        n1, n2 = n2, n1 + n2

    return fibonacci_sequence


print(compute_fibonacci(given_num))

# Time complexity: O(n)
