# Compute the Fibonacci sequence up to a given number.


given_num = 20


def compute_fibonacci(N):
    n1 = 0
    n2 = 0
    n_res = 1
    fibonacci_sequence = [0, 1]
    if N > 2:
        for num in range(2, N):
            n1 = n2
            n2 = n_res
            n_res = n1 + n2
            fibonacci_sequence.append(n_res)
        return fibonacci_sequence
    elif N == 2:
        return fibonacci_sequence
    elif N == 1:
        fibonacci_sequence = [0]
        return fibonacci_sequence
    elif N <= 0:
        fibonacci_sequence = []
        return fibonacci_sequence


print(compute_fibonacci(given_num))

# Time complexity: O(n)
