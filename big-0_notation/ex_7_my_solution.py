# Merge two lists, which can be sorted or unsorted, into one sorted list.


list_1 = [2, 4, 6, 8]
list_2 = [1, 3, 5, 7]

list_3 = [5, 10, 15, 20, 25]
list_4 = [2, 7, 12, 17, 22, 27]

list_5 = [4, 2, 8, 6]
list_6 = [7, 1, 5, 3]

list_7 = [15, 5, 20, 10, 25]
list_8 = [27, 12, 22, 7, 17, 2]

list_9 = [15, 5, -55, 10, -8]
list_10 = [27, 3.14, 22, 7, -2.8, 2]


def sort_and_merge(list_1, list_2):
    merged_list = list_1 + list_2
    merged_list.sort()
    return merged_list


print(sort_and_merge(list_9, list_10))

# Time complexity: O((n + m) log(n + m))
