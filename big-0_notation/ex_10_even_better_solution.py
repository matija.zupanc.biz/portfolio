# Given a list of integers, find the pair of elements that sum to a specific
# target.


list_1 = [2, 4, 6, 8, 10, 10, 2]
list_2 = [1, -3, -11, 4, -5]
list_3 = [3, 1, 4, 1, 5, 9, 2, 6, 5]
list_4 = []
list_5 = [7]

target_1 = 12
target_3 = 1
target_4 = 0
target_5 = -16


def sum_of_pair(nums, target):
    if len(nums) < 2:
        return "List too small"

    result = []

    for i in range(len(nums)):
        for j in range(i + 1, len(nums)):
            if nums[i] + nums[j] == target:
                result.append([nums[i], nums[j]])

    return result


print(sum_of_pair(list_1, target_1))

# Time complexity: O(n^2)
