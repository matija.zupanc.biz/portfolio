# Remove duplicates from a list.


my_list_1 = [1, 2, 3, 4, 5]
my_list_2 = [5, 4, 3, 2, 1]
my_list_3 = [3, 1, 4, 1, 5, 9, 2, 6, 5]
my_list_4 = []
my_list_5 = [7]
my_list_6 = ['a', 'b', 'c', 'd', 'e']
my_list_7 = ['e', 'd', 'c', 'b', 'a']
my_list_8 = ['a', 'b', 'c', 'a', 'b', 'c', 'b']
my_list_9 = ['apple', 'banana', 'cherry', 'date', 'elderberry']
my_list_10 = [
    'elderberry', 'date', 'cherry', 'banana', 'cherry', 'apple', 'cherry']
my_list_11 = ['apple', 4, 'banana', 'apple', 'cherry', '4', 'elderberry', 4]


def remove_duplicates(my_list):
    unique_elements = set()
    result = []

    for el in my_list:
        if el not in unique_elements:
            unique_elements.add(el)
            result.append(el)

    return result


print(my_list_2)
print(remove_duplicates(my_list_2))

# Time complexity: O(n)
