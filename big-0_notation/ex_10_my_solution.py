# Given a list of integers, find the pair of elements that sum to a specific
# target.


list_1 = [2, 4, 6, 8, 10, 10, 2]
list_2 = [1, -3, -11, 4, -5]
list_3 = [3, 1, 4, 1, 5, 9, 2, 6, 5]
list_4 = []
list_5 = [7]

target_1 = 12
target_3 = 1
target_4 = 0
target_5 = -16


# Has a bug - while performing addition it will also add itself and if the sum
# of that is the target number it will consider it a viable result (resolved
# in ex_10_even_better_solution)
def sum_of_pair(list_nums, target):
    if len(list_nums) < 2:
        result = 'List too small'
        return result

    result = []
    for num in list_nums:
        for num_2 in list_nums:
            sum = num + num_2
            if sum == target and [num_2, num] not in result:
                result.append([num, num_2])

    return result


print(sum_of_pair(list_1, target_1))

# Time complexity: O(n^2)
