# Check if a number is prime.

number_0 = 23
number_1 = 34
number_2 = 2
number_3 = 1
number_4 = 0
number_5 = 7
number_6 = 8.0
number_7 = 6.4
number_8 = 17.3
number_9 = -7
number_10 = 1000001  # Not a prime


def is_prime(number):
    if type(number) != int or number < 2:
        return print('NO PRIME')

# Didn't do this correctly - had to look on the interwebs to find how to
# calculate prime numbers
    for i in range(2, int(number/2)+1):
        if number % i == 0:
            return print('NO PRIME')
    else:
        return print('Yes PRIME')


is_prime(1000001)

# Time complexity:  O(sqrt(n))
