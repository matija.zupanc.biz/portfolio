# Reverse a string.


example_string_1 = 'straight'
example_string_2 = 'reverse'
example_string_3 = 'madam'
example_string_4 = '17'
example_string_5 = 7263
example_string_6 = ''


def reverse_string(string):
    list_string = list(str(string))
    list_string.reverse()
    reverse_string = ''
    for i in list_string:
        reverse_string += i

    return reverse_string


print(reverse_string(example_string_5))

# Time complexity: O(n)
