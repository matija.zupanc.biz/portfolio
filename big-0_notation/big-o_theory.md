# [Big-O](https://trello.com/c/rGhaNk8k/184-1-what-is-big-o-notation)

## 1. What is Big-O Notation?

The Big-O Notation is a mathematical concept used in computer science and other fields to describe the performance of an algorithm in terms of the size of its input. It provides an upper bound on the growth rate of an algorithm's time complexity as the input size increases to infinity. In simpler terms, it gives us an idea of how well an algorithm scales as the input size increases. The notation is expressed using the "O" symbol, followed by a function that describes the upper bound of the algorithm's time complexity in terms of the input size.

## 2. How is Big-O Notation used in computer science?

In computer science, Big-O Notation is used to analyze the efficiency of algorithms and to compare different algorithms to see which one is more efficient. By analyzing the time complexity of an algorithm using Big-O Notation, we can estimate how long it will take for the algorithm to run for large input sizes. This information is crucial in many applications, such as optimizing database queries, sorting large datasets, and processing complex data structures.

Big-O Notation is also used to determine the scalability of a system. Scalability is the ability of a system to handle increasing workloads without slowing down or failing. By analyzing the time complexity of an algorithm using Big-O Notation, we can estimate how much computational resources the algorithm will require as the input size increases, which is essential for ensuring the scalability of a system.

## 3. What is the difference between time complexity and space complexity?

* **Time complexity** - measures how the running time of an algorithm grows as the size of the input grows. It focuses on how long it takes for an algorithm to produce a result, and it is typically expressed in terms of the number of operations or steps performed by the algorithm as a function of the input size. Time complexity is usually expressed using Big-O notation, which provides an upper bound on the growth rate of the running time as the input size increases.

* **Space complexity** -  measures the amount of memory or storage required by an algorithm to run as a function of the input size. It focuses on how much memory an algorithm needs to store data structures or intermediate results, and it is typically expressed in terms of the number of variables or data structures used by the algorithm as a function of the input size. Space complexity is also expressed using Big-O notation, which provides an upper bound on the growth rate of the memory usage as the input size increases.

## 4. What is the worst-case time complexity of an algorithm?

The worst-case time complexity of an algorithm is the maximum amount of time an algorithm can take to complete its task for any input of size n. In other words, it measures the upper bound on the running time of the algorithm for the worst possible input.

The worst-case time complexity is an important measure of an algorithm's performance because it ensures that the algorithm will always terminate and produce a correct output, regardless of the input data. However, it also provides a pessimistic estimate of the algorithm's running time because it assumes that the input data will always be in the worst possible configuration for the algorithm.

For example, consider a sorting algorithm that has a worst-case time complexity of O(n^2). This means that the algorithm takes no more than c * n^2 steps for any input of size n, where c is some constant. In other words, the algorithm is guaranteed to sort any input list, but it may take a long time to do so if the list is very large or contains many duplicates.

## 5. What is the best-case time complexity of an algorithm?

The best-case time complexity of an algorithm is the minimum amount of time an algorithm can take to complete its task for any input of size n. In other words, it measures the lower bound on the running time of the algorithm for the best possible input.

It is usually not very informative on its own, because it may not reflect the typical or average case behavior of the algorithm. In practice, developers often focus on the worst-case or average-case time complexity of an algorithm, rather than the best-case time complexity.

For example, consider a sorting algorithm that has a best-case time complexity of O(n). This means that the algorithm can sort an already-sorted list in linear time, without having to perform any swaps or comparisons. However, in practice, the input list may not be already sorted, and the algorithm may have to perform many more comparisons and swaps, leading to a higher actual running time.

## 6. What is the average-case time complexity of an algorithm?

The average-case time complexity of an algorithm is the expected amount of time the algorithm takes to complete its task for a random input of size n, averaged over all possible inputs of size n. In other words, it measures the typical or average performance of the algorithm, taking into account the probability distribution of the input data.

Calculating the average-case time complexity of an algorithm can be more challenging than calculating the worst-case time complexity, because it requires knowledge of the statistical distribution of the input data and the behavior of the algorithm under that distribution.

The average-case time complexity is an important measure of an algorithm's performance, because it can provide a more realistic estimate of how the algorithm will perform in practice, given the typical distribution of the input data. However, it is worth noting that the average-case time complexity may not always be a good predictor of the actual running time of an algorithm, because it assumes a random distribution of the input data, whereas real-world input data may be biased or skewed in ways that affect the algorithm's performance.

## 7. What is the difference between O(1), O(log n), O(n), O(n log n), O(n^2), and O(2^n) time complexities?

1. **O(1)** - Constant Time Complexity:
    * An algorithm with O(1) time complexity has a constant time requirement regardless of the input size. It means that the runtime of the algorithm remains the same, regardless of how large the input is.

2. **O(log n)** - Logarithmic Time Complexity:
    * An algorithm with O(log n) time complexity has a runtime that increases logarithmically with the input size. As the input size grows, the algorithm's runtime increases at a slower rate.

3. **O(n)** - Linear Time Complexity:
    * An algorithm with O(n) time complexity has a runtime that increases linearly with the input size. If the input size doubles, the runtime also doubles.

4. **O(n log n)** - Linearithmic Time Complexity:
    * An algorithm with O(n log n) time complexity has a runtime that grows in proportion to the input size multiplied by the logarithm of the input size. It's commonly seen in efficient sorting algorithms like Merge Sort and Quick Sort.

5. **O(n^2)** - Quadratic Time Complexity:
    * An algorithm with O(n^2) time complexity has a runtime that increases quadratically with the input size. If the input size doubles, the runtime increases fourfold.

6. **O(2^n)** - Exponential Time Complexity:
    * An algorithm with O(2^n) time complexity has a runtime that grows exponentially with the input size. As the input size increases even slightly, the runtime grows very rapidly.

In general, as we move from O(1) to O(log n) to O(n) to O(n log n) to O(n^2) to O(2^n), the time complexity increases, indicating a higher computational cost and potentially slower performance. It's important to note that these are just a few examples of common time complexities, and there are other notations to describe different growth rates as well.

## 8. What is the difference between a linear and a non-linear algorithm?

The difference between a linear and a non-linear algorithm lies in the way they process and handle data.

* **Linear Algorithm**:

  * A linear algorithm is characterized by its execution time or behavior that is directly proportional to the size of the input. In other words, as the input size increases, the time required or the number of operations performed by the algorithm increases linearly.

  * Examples of linear algorithms include traversing a list or array, performing a simple search, or iterating through a collection of elements one by one.

  * The time complexity of linear algorithms is often expressed as O(n), where 'n' represents the input size.

* **Non-linear Algorithm**:

  * A non-linear algorithm does not have a direct correlation between the execution time and the input size. The execution time can vary in different ways, and it is not strictly proportional to the input size.

  * Non-linear algorithms often involve complex data structures or intricate processing logic that results in more sophisticated behavior than linear algorithms.

  * Examples of non-linear algorithms include binary search, sorting algorithms like quicksort and mergesort, graph traversal algorithms like depth-first search (DFS) and breadth-first search (BFS), and algorithms that operate on tree structures.

  * The time complexity of non-linear algorithms can vary significantly and is expressed using different notations like O(log n), O(n log n), O(n^2), etc.

In summary, a linear algorithm has a simple and direct relationship between the input size and the execution time, resulting in a linear growth pattern. On the other hand, a non-linear algorithm exhibits more complex behavior, with execution times that can grow differently as the input size increases, often involving more advanced data structures and algorithms.

## 9. How can you improve the time complexity of an algorithm?

Improving the time complexity of an algorithm involves optimizing its performance to reduce the number of operations or decrease the growth rate of the execution time as the input size increases. Here are some common approaches to improve time complexity:

1. **Algorithmic Optimization**:
    * Analyze the algorithm's logic and look for opportunities to simplify or optimize the steps involved. This can include removing unnecessary computations, reducing redundant operations, or finding more efficient algorithms for the problem.

    * Consider alternative data structures or algorithms that can solve the problem more efficiently. For example, replacing a linear search with a binary search for sorted data can significantly improve the time complexity.

2. **Efficient Data Structures**:
    * Choose appropriate data structures that can perform the required operations efficiently. Different data structures have different time complexities for specific operations. For example, using a hash table (dictionary) can provide constant-time lookup compared to linear-time lookup in a list for certain cases.

3. **Memoization**:
    * Utilize memoization techniques to store the results of expensive function calls and reuse them instead of recomputing. This can be particularly useful in recursive algorithms or dynamic programming problems, where overlapping subproblems exist.

4. **Divide and Conquer**:
    * Break down a problem into smaller subproblems and solve them independently. This approach is often used in algorithms like merge sort and quicksort. By dividing the problem into smaller parts, the overall time complexity can be reduced.

5. **Time-Space Trade-off**:
    * Consider trading off additional space complexity for improved time complexity. Sometimes, precomputing or caching intermediate results can speed up subsequent computations, reducing the overall execution time.

6. **Parallelism**:
    * Explore opportunities for parallel execution by dividing the workload among multiple threads or processors. This approach can be beneficial when the algorithm can be parallelized effectively.

It's important to note that improving time complexity is not always possible or necessary in every scenario. The choice of algorithm and optimization techniques depends on the specific problem, input size, and constraints. Balancing time complexity improvements with other factors like code readability, maintainability, and space complexity is crucial in algorithm design and optimization.

## 10. What is the time complexity of a loop that runs n times?

A loop that runs exactly n times has a time complexity of O(n). This means that the execution time of the loop is directly proportional to the size of the input n. As n increases, the loop will execute n iterations, resulting in a linear growth in the execution time.

## 11. What is the time complexity of nested loops?

The time complexity of nested loops depends on the number of nested levels and the number of iterations each loop performs. Let's consider a few scenarios:

1. **Single Nested Loop**:
    * If you have one loop nested inside another loop, the time complexity is generally expressed as O(n*m), where 'n' is the number of iterations of the outer loop and 'm' is the number of iterations of the inner loop. The total number of iterations will be the product of 'n' and 'm'.

2. **Multiple Nested Loops**:
    * For multiple levels of nested loops, the time complexity can be expressed as the product of the number of iterations of each loop. For example, if you have three nested loops with 'n', 'm', and 'k' iterations respectively, the time complexity would be O(n_m_k).

3. **Nested Loops with Variable Iterations**:
    * If the number of iterations for each loop depends on the input size or some other variable, the time complexity analysis becomes more complex. In such cases, it's important to carefully analyze the behavior of the loops to determine the overall time complexity.

It's important to note that the time complexity is determined by the dominant factor. So, if you have nested loops but the number of iterations decreases significantly with each level, the dominant factor might not be the number of loops but some other factor within the loop body.

Analyzing the time complexity of nested loops can be challenging, especially when the iterations are not straightforward or vary based on conditional statements. In such cases, it's important to carefully analyze the loop structure, the iterations, and any conditional statements to determine the overall time complexity.

## 12. How do you analyze the time complexity of a recursive algorithm using Big-O Notation?

Analyzing the time complexity of a recursive algorithm involves considering the number of recursive calls made and the work done in each recursive call. Here's a general approach to analyzing the time complexity of a recursive algorithm using Big-O Notation:

1. **Identify the Recurrence Relation**:
    * Determine the recursive relation or formula that describes the relationship between the input size and the number of recursive calls made. This relation defines how the problem is divided into smaller subproblems.

2. **Define the Base Case**:
    * Identify the base case(s) of the recursion, which represents the simplest or smallest version of the problem that can be directly solved without further recursion.

3. **Solve or Simplify the Recurrence Relation**:
    * Solve or simplify the recurrence relation to express it in terms of smaller subproblems until reaching the base case. This may involve breaking down the problem size or reducing it by a certain factor with each recursive call.

4. **Determine the Time Complexity**:
    * Analyze the work done in each recursive call and the number of recursive calls made to determine the overall time complexity.

    * Express the time complexity using Big-O Notation by identifying the dominant term or factor that grows the fastest as the problem size increases.

Example in trello.

## 13. Explain the concept of amortized analysis in the context of time complexity

**Amortized analysis** is a technique used to analyze the time complexity of algorithms over a series of operations, taking into account both costly and inexpensive operations. It provides a way to distribute the cost of expensive operations across multiple cheaper operations, resulting in a more balanced average cost per operation.

The concept of amortized analysis is particularly useful when dealing with data structures or algorithms that have occasional costly operations but are usually efficient. It helps us understand the overall performance of an algorithm by considering the aggregate behavior of a sequence of operations, rather than focusing solely on individual operations.

There are three common methods for performing amortized analysis:

1. **Aggregate Analysis**:
    * This method involves determining the total cost of a sequence of operations and then dividing it by the number of operations to obtain the average cost per operation. It assumes that the expensive operations are offset by the inexpensive ones, leading to an average cost that is lower than the worst-case cost.

2. **Accounting Method**:
    * In this method, we assign different costs to operations based on their actual and perceived expenses. We overcharge some operations and store the excess charges as "credits" that can be used to pay for expensive operations in the future. By carefully designing the cost assignments, we ensure that the accumulated credits are sufficient to cover the costlier operations, resulting in an amortized constant time complexity per operation.

3. **Potential Method**:
    * The potential method involves defining a "potential function" that quantifies the potential energy or accumulated potential of the data structure at any given state. The potential function is used to assign costs to operations based on the changes in potential energy. The difference between the potential after an operation and before the operation represents the amortized cost of the operation.

Amortized analysis provides a more accurate understanding of the performance of algorithms and data structures by considering their long-term behavior. It helps identify cases where costly operations are balanced by subsequent inexpensive operations, resulting in an overall efficient algorithm with a favorable average time complexity.

## 14. Explain the difference between in-place sorting algorithms and out-of-place sorting algorithms

The difference between in-place sorting algorithms and out-of-place sorting algorithms lies in how they handle memory during the sorting process.

1. **In-Place Sorting Algorithms**:
    * In-place sorting algorithms are designed to sort the elements of an array or a data structure by rearranging the elements within the original input space, without requiring additional memory beyond a few variables. These algorithms modify the input data structure directly without creating a separate copy.The advantage of in-place sorting algorithms is that they conserve memory because they do not need additional space proportional to the input size. They are efficient in terms of space complexity. However, they might require additional computational steps or have higher time complexity compared to out-of-place algorithms.Examples of in-place sorting algorithms include:
        * Bubble Sort

        * Insertion Sort

        * Selection Sort

        * Quicksort (in its standard implementation)

        * Heapsort (using the input array as the heap data structure)

2. **Out-of-Place Sorting Algorithms**:
    * Out-of-place sorting algorithms, also known as "extra-space" or "auxiliary-space" sorting algorithms, sort the elements of the input data structure by creating a separate copy of the input and then manipulating the copy. The sorted output is stored in the new memory space, while the original input remains unmodified.The advantage of out-of-place sorting algorithms is that they guarantee the integrity of the original data and can be more straightforward to implement. They may also offer better time complexity compared to in-place algorithms, as they can optimize the sorting process by utilizing additional memory.Examples of out-of-place sorting algorithms include:
        * Merge Sort

        * Counting Sort

        * Radix Sort

        * Bucket Sort

It's worth noting that the choice between in-place and out-of-place sorting algorithms depends on the specific requirements and constraints of the problem at hand. If memory efficiency is a concern, in-place algorithms may be preferred. However, if maintaining the original data intact or optimizing for time complexity is crucial, out-of-place algorithms may be a better choice.
