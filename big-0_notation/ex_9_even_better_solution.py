# Check if a number is prime.

from sympy import isprime

number_0 = 23
number_1 = 34
number_2 = 2
number_3 = 1
number_4 = 0
number_5 = 7
number_6 = 8.0
number_7 = 6.4
number_8 = 17.3
number_9 = -7
number_10 = 1000001  # Not a prime

print(isprime(number_10))


# Time complexity: O(sqrt(n))
