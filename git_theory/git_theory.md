# [Git](https://trello.com/c/2qTK6PbO/200-1-what-is-git-and-what-are-its-advantages-over-other-version-control-systems)

## 1. What is Git and what are its advantages over other version control systems?

Git is a distributed version control system (VCS) designed to track changes in files and coordinate work among multiple developers. It offers several advantages over other version control systems:

1. **Distributed nature**: Git allows each developer to have a complete local copy of the entire repository, including its full history. This enables developers to work offline and independently, making it easier to experiment, branch, and merge changes.

2. **Fast and efficient**: Git is built to be highly performant, even with large repositories and extensive history. It uses advanced algorithms for storing and retrieving data, making operations like commit, branching, and merging fast and efficient.

3. **Branching and merging**: Git has robust branching and merging capabilities. Creating a new branch is a lightweight operation, and Git provides powerful tools to merge branches together, allowing for parallel development and easy collaboration.

4. **Data integrity**: Git uses cryptographic hashing to ensure the integrity of data. Every commit is identified by a unique hash, which makes it virtually impossible to accidentally modify or lose data.

5. **Commit history and tracking**: Git maintains a detailed history of all changes made to the repository, including who made the changes and when. This comprehensive tracking allows for easy identification of modifications, rollbacks, and debugging.

6. **Collaboration and remote repositories**: Git facilitates collaboration among developers through remote repositories, such as GitHub or GitLab. It enables seamless sharing, reviewing, and merging of code changes between multiple contributors.

7. **Wide adoption and community support**: Git has gained widespread adoption in the software development community. It is used by numerous organizations, open-source projects, and individual developers. This popularity has resulted in a large and active community that provides extensive support, resources, and tools for using Git effectively.

8. **Flexibility and extensibility**: Git is highly flexible and can be customized to suit different project needs. It supports various workflows and branching strategies, allowing teams to adapt Git to their specific development processes. Git also has a rich ecosystem of plugins and extensions that enhance its functionality and integration with other tools.

9. **Integration with online platforms**: Git integrates seamlessly with popular online platforms such as GitHub, GitLab, and Bitbucket. These platforms provide hosting services for Git repositories and offer features like issue tracking, pull requests, and code review, enhancing collaboration and team workflows.

Overall, Git's flexibility, speed, and powerful features make it a popular choice for version control in software development projects of all sizes.

## 2. How do you initialize a new Git repository?

To initialize a new Git repository, you can follow these steps:

1. Open a command-line interface (such as Terminal or Command Prompt) in the directory where you want to create the repository.

2. Use the command to initialize the repository.

    * `git init`

3. Git will create a new, empty Git repository in the current directory. It will create a hidden directory named .git, which contains the necessary files and subdirectories for version control.

4. Your new Git repository is now initialized and ready to use. You can start adding files, making commits, and utilizing Git's version control features.

It's important to note that the git init command initializes a local repository on your machine. If you want to connect it to a remote repository (e.g., on GitHub or GitLab), you'll need to set up the remote connection separately using commands like git remote add origin `<remote-repository-url>`.

## 3. What is the difference between Git and GitHub?

Git and GitHub are related but serve different purposes:

1. **Git**: Git is a distributed version control system (VCS) designed to track changes in files and coordinate work among multiple developers. It is a command-line tool that runs locally on your computer. Git allows you to create repositories, track changes, create branches, merge code, and maintain a complete history of your project.

2. **GitHub**: GitHub is a web-based platform that provides hosting services for Git repositories. It adds a centralized remote repository on the internet, making it easier to collaborate with others and access your code from different locations. GitHub offers a graphical user interface (GUI) and a range of additional features such as issue tracking, pull requests, code review, project management tools, and integration with various development workflows.

In summary, Git is the version control system itself, while GitHub is a web-based hosting service that utilizes Git for version control. Git is used locally on your computer to manage the version control of your project, whereas GitHub provides a platform to store, share, and collaborate on Git repositories over the internet. Many developers use Git locally and then push their code to GitHub for centralized storage and collaboration. However, it's important to note that there are other Git hosting platforms besides GitHub, such as GitLab and Bitbucket.

## 4. What is a commit in Git?

In Git, a commit represents a snapshot of the changes made to a repository at a specific point in time. It is a fundamental concept in Git and serves as a way to track and record modifications to files.

When you make changes to files in a Git repository, you need to explicitly stage and commit those changes to create a new commit. Each commit in Git consists of the following elements:

1. **Snapshot of changes**: A commit contains a snapshot of the files in the repository at the time of the commit. It records the changes made to the tracked files since the previous commit.

2. **Unique identifier**: Each commit is identified by a unique hash, typically a SHA-1 hash, which is generated based on the content of the commit. This hash allows for efficient tracking and referencing of commits.

3. **Commit message**: A commit includes a message that describes the changes made in the commit. It provides a brief summary of the modifications and helps other developers understand the purpose and context of the commit.

4. Author and timestamp**: Git records the author's name and email address, as well as the timestamp when the commit was created. This information helps identify who made the changes and when.

Commits in Git form a directed acyclic graph, where each commit points to its parent commit(s), forming a chronological history of changes. This allows you to traverse the commit history, view previous versions of files, compare changes, and easily revert or merge commits.

By creating commits, Git enables you to track the evolution of your project, collaborate with others, and safely manage changes over time.

## 5. How do you merge two branches in Git?

To merge two branches in Git, you can follow these steps:

1. Ensure you are in the branch where you want to merge the changes. You can use the `git branch` command to see the current branch and switch to the desired branch using `git checkout <branch-name>` if needed.

2. Execute the following command to merge another branch into the current branch:

    * `git merge <branch-to-merge>`

    * replace `<branch-to-merge>` with the name of the branch you want to merge into the current branch

3. Git will attempt to automatically merge the changes from the specified branch into the current branch. If the changes are non-conflicting, Git will perform a "fast-forward" merge, where the current branch simply moves forward to include the commits from the other branch.

4. If there are conflicts between the branches, Git will prompt you to resolve the conflicts manually. Conflicts occur when the same lines of code in the same files have conflicting changes in the merging branches. You need to edit the conflicting files, resolve the conflicts, and save the changes.

5. After resolving any conflicts, stage the changes using `git add <resolved-file>` for each resolved file.

6. Once the conflicts are resolved and changes are staged, complete the merge by running the `git commit` command. Git will automatically generate a commit message summarizing the merge.

7. After the merge commit is created, the two branches are effectively combined, and the changes from the merged branch are now part of the current branch.

It's important to note that when merging branches, it's a good practice to ensure the branch being merged is up to date with the latest changes from the branch you are merging into. You can use the `git pull` command to fetch and incorporate the latest changes before performing the merge.

By merging branches in Git, you can combine separate lines of development, integrate features, and incorporate changes from other branches into your current branch.

## 6. What is a conflict in Git and how do you resolve it?

A conflict in Git occurs when Git is unable to automatically merge changes from two different branches due to conflicting modifications in the same part of a file. Conflicts typically arise when multiple developers make conflicting changes to the same lines of code or when different branches modify the same file independently.

When a conflict occurs, Git marks the affected file(s) with conflict markers (`<<<<<<<`, `=======`, and `>>>>>>>`) to indicate the conflicting sections. Resolving conflicts involves manually editing the conflicted file(s) to determine the desired final state of the code. Here's how you can resolve conflicts in Git:

1. **Identify conflicts**: Run the `git status` command to check for any conflicted files. Git will list the files that have conflicts.

2. **Open the conflicted file(s) in a text editor**. Inside the file, you'll find the conflict markers (`<<<<<<<`, `=======`, and `>>>>>>>`) surrounding the conflicting code sections.

3. **Review the conflict**: Between the `<<<<<<<` and `=======` markers, you'll see the conflicting changes from the current branch, and between the `=======` and `>>>>>>>` markers, you'll see the conflicting changes from the branch being merged. Analyze the conflicting sections to understand the conflicting modifications.

4. **Edit the file**: Manually modify the conflicted file to resolve the conflict. You can choose to keep one version of the code, combine the changes, or rewrite the code entirely, depending on your requirements.

5. **Remove conflict markers**: Delete the conflict markers (`<<<<<<<`, `=======`, and `>>>>>>>`) once you have resolved the conflict and made the necessary changes. Ensure that the final state of the file is what you want it to be.

6. **Save the file**: Save the changes made to the file.

7. **Add and commit the resolved file**: After resolving all conflicts in the file, add the file to the staging area using `git add <resolved-file>`. Once all conflicts are resolved and staged, commit the changes using `git commit`.

It's important to note that conflict resolution is a manual process that requires careful consideration and understanding of the conflicting changes. Collaboration and communication with other developers are often necessary to ensure the conflicts are resolved correctly.

After resolving conflicts, you can continue with the merge or rebase process to complete the branch integration in Git.

## 7. What is the purpose of the "git clone" command?

[Clone_with_SSH](https://www.youtube.com/watch?v=iXuIp5uNnLk)

The `git clone` command in Git is used to create a copy of a remote repository onto your local machine. It allows you to retrieve the entire repository, including all its files, commit history, and branches.

The purpose of the `git clone` command is to set up a local working copy of a repository for various reasons, such as:

1. **Collaborating on a project**: When you want to contribute to a project hosted on a remote Git repository (such as GitHub, GitLab, or Bitbucket), you can use `git clone` to create a local copy of the repository. This allows you to work on the code, make changes, and propose them back to the project by creating pull requests or pushing your commits to the remote repository.

2. **Working on your own project**: If you have an existing remote repository or want to start a new project, you can use `git clone` to create a local copy of the repository on your machine. This enables you to work on the project locally, make changes, and version control your code using Git.

3. **Obtaining a reference to an open-source project**: If you want to explore or use an open-source project hosted on a remote repository, `git clone` allows you to obtain a local copy of the project's codebase. This gives you the ability to inspect the code, run it locally, and potentially contribute to the project.

The `git clone` command not only copies the files and history from the remote repository to your local machine but also sets up the necessary connections to the remote repository, enabling you to fetch and push changes.

After executing the `git clone` command, Git creates a new directory with the name of the repository and initializes it as a local Git repository. You can then navigate into the newly created directory and start working with the code and commit history using Git commands.

## 8. How do you revert a commit in Git?

`git revert <commit-hash>`

(Replace `<commit-hash>` with the hash of the commit you want to revert. Alternatively, you can specify a branch name, tag, or other references instead of the commit hash.)

To revert a commit in Git, you can use the `git revert` command. Reverting a commit creates a new commit that undoes the changes introduced by the original commit while keeping a record of the reversion. Here's how you can revert a commit:

1. Identify the commit you want to revert: Obtain the commit hash or identify the commit relative to the HEAD or branch name. You can use commands like `git log` or `git reflog` to view the commit history and find the appropriate commit.

2. Git will open a text editor to create a commit message for the reversion. Modify the message if necessary and save the file.

3. After saving the commit message, Git will create a new commit that undoes the changes made by the reverted commit. The commit history will reflect the reversion as a new commit.

It's important to note that the `git revert` command creates a new commit that undoes the changes, instead of erasing the original commit. This is a safer approach to preserve the commit history and avoid losing any work. The reverted commit remains in the history, and future merges or rebases will take into account the reversion.

After reverting a commit, it's advisable to push the changes to the remote repository using `git push` if you want the reversion to be reflected in the remote repository as well.

By using git revert, you can safely undo changes introduced by a commit without altering the commit history or affecting other collaborators.

## 9. What is the purpose of the "git stash" command?

The `git stash` command in Git allows you to save changes that you have made in your working directory, but are not ready to commit yet. It enables you to temporarily set aside your modifications and switch to a different branch or work on a different task without committing incomplete or experimental changes. The purpose of the `git stash` command is to provide a way to save and manage these unfinished changes.

Here's how the `git stash` command works:

1. Execute the `git stash` command to stash your changes. Git will save your modifications, including both staged and unstaged changes, and revert your working directory to the state of the last commit.

2. After stashing your changes, you can switch to another branch using `git checkout` or perform any other Git operations.

3. To retrieve your stashed changes, use the `git stash apply` command. This command reapplies the most recent stash to your current branch, restoring the changes you stashed. The stashed changes remain in the stash stack.

4. If you have multiple stashes, you can specify which stash to apply by using the stash index. EX: `git stash apply stash@{2}`.

5. Once you have applied the stashed changes, you can continue working on them, make further modifications, stage files, and commit as needed.

Additional git stash commands include:

* `git stash list`: Lists all the stashes in the stash stack, showing their index and description.

* `git stash drop`: Deletes the most recent stash from the stash stack.

* `git stash pop`: Retrieves and applies the most recent stash, then removes it from the stash stack.

* `git stash branch <branch-name>`: Creates a new branch based on the most recent stash, applies the stash to the new branch, and switches to it.

The `git stash` command is useful when you need to temporarily store and retrieve changes without committing them, allowing you to switch tasks or branches seamlessly. It provides a flexible way to manage and organize your work in progress.

## 10. What is the "git rebase" command used for?

The `git rebase` command in Git is used to incorporate changes from one branch onto another branch. It allows you to modify the commit history of a branch by moving, combining, or modifying commits. The primary purpose of the `git rebase` command is to integrate changes from one branch onto another in a more streamlined and linear fashion.

Here's how the `git rebase` command works:

1. Identify the branch onto which you want to apply the changes. This is usually the target branch where you want to bring in the changes from another branch.

2. Checkout the branch you want to rebase onto:

    * `git checkout <target-branch>`

3. Execute the following command to initiate the rebase:

    * `git rebase <source-branch>`

    * Replace `<source-branch>` with the branch that contains the changes you want to incorporate.

4. Git will apply the commits from the `<source-branch>` onto the `<target-branch>`, one commit at a time. If there are conflicts, Git will pause the rebase process and prompt you to resolve the conflicts manually, similar to resolving conflicts during a merge operation.

5. After resolving conflicts, stage the changes using `git add <resolved-file>` for each resolved file.

6. Once all conflicts are resolved and changes are staged, continue the rebase process by running:

    * `git rebase --continue`

    * Git will apply the remaining commits from the `<source-branch>` onto the `<target-branch>`. If there are more conflicts, Git will pause again for conflict resolution.

7. After the rebase is complete, the commits from the `<source-branch>` will be integrated into the `<target-branch>` in a linear fashion, following the commit order of the `<source-branch>`.

It's important to note that when you rebase a branch, you are essentially creating new commits based on the original ones. This can rewrite the commit history of the branch and should be used with caution, especially if the branch is shared or has been pushed to a remote repository.

Some additional `git rebase` options include `--interactive (-i)` for interactive rebasing, which allows you to modify, reorder, squash, or split commits during the rebase process.

The `git rebase` command is useful when you want to integrate changes from one branch onto another while maintaining a clean and linear commit history. It can be used to incorporate feature branches, update branches with the latest changes, or tidy up the commit history before merging.

## 11. How do you delete a branch in Git?

To delete a branch in Git, you can use the `git branch` command with the `-d` or `-D` option. Here's how you can delete a branch:

1. Check the list of branches in your repository using the following command:

    * `git branch`

2. Identify the branch you want to delete from the list. Ensure that you are currently on a different branch, as you cannot delete the branch you are currently on.

3. Delete the branch using one of the following commands:

    * To delete a branch that has been fully merged into the current branch, use:
        * `git branch -d <branch-name>`

    * To forcefully delete a branch, regardless of whether it has been merged or not, use:
        * `git branch -D <branch-name>`

    * This option is useful when you want to delete a branch even if there are unmerged changes.

4. After executing the command, Git will delete the specified branch.

It's important to note that when you delete a branch, the branch's commit history is not deleted. All the commits that were part of the deleted branch are still accessible and retained in the repository. Deleting a branch only removes the label or pointer associated with that branch.

Deleting branches that are no longer needed helps keep your repository organized and reduces clutter. However, be cautious when deleting branches, especially if they contain unmerged changes or if they are shared with other developers. Always double-check that you are deleting the intended branch to avoid accidental data loss.

Additionally, you can also delete a branch remotely by using the `git push` command with the `--delete` option. For example:

* `git push origin --delete <branch-name>`

This command deletes the specified branch from the remote repository.

## 12. How do you view the commit history in Git?

In Git, you can view the commit history of a repository using the `git log` command. The `git log` command displays a chronological list of commits in reverse order, with the most recent commit appearing first.

* `git log`

This will display the commit history, starting from the latest commit and going backwards. By default, the `git log` command shows the commit hash, author, date, and commit message for each commit.

You can customize the output of the `git log` command by using various options. Here are some commonly used options:

* `--oneline`: Shows a condensed view of the commit history, displaying each commit on a single line.

* `--graph`: Displays a text-based graph of the commit history, illustrating the branching and merging of branches.

* `--decorate`: Shows additional information such as branch and tag names associated with each commit.

* `--author=<author-name>`: Filters the commit history based on the specified author's name.

* `--since=<date>`: Shows commits since the specified date or time range.

* `--until=<date>`: Shows commits up until the specified date or time range.

* `--grep=<pattern>`: Filters the commit history based on a specified pattern or keyword in the commit message.

You can combine multiple options to further refine the output and tailor it to your needs. For example:

* `git log --oneline --graph --decorate`

The `git log` command provides valuable information about the commit history, including commit messages, authors, dates, and related branches or tags. It helps you understand the progression of the project, review changes made by collaborators, and navigate the history for tasks like checking out previous commits or identifying the introduction of specific features or bugs.

## 13. What is a remote repository in Git?

In Git, a remote repository refers to a repository that is hosted on a different location or server than your local repository. It acts as a central location where you can share your code and collaborate with other developers. Remote repositories are commonly used when working in a team or when contributing to open-source projects.

Here are a few key points about remote repositories:

1. **Collaboration**: Remote repositories facilitate collaboration by allowing multiple developers to work on the same project. Each developer can clone the remote repository to their local machine, make changes, and push those changes back to the remote repository for others to see and merge into their own local copies.

2. **Centralized storage**: Remote repositories serve as a central storage location for your project's code. They provide a single source of truth that team members can synchronize with and access from anywhere, eliminating the need for physical file transfers or manual synchronization.

3. **Sharing and distribution**: Remote repositories make it easy to share your code with others. By pushing your local commits to the remote repository, you make them available to other team members or contributors. Similarly, you can fetch or pull changes made by others from the remote repository to update your local copy.

4. **Backup and recovery**: Remote repositories act as a backup of your codebase. If you accidentally delete or lose your local repository, you can simply clone the remote repository again to retrieve the code and its commit history.

5. **Remote branches**: Remote repositories include remote branches that mirror the branches in the local repository. These branches represent the different development lines or features being worked on by various team members. You can push your local branch to a remote repository to make it accessible to others, or fetch and merge changes from remote branches into your local branches.

Popular remote repository hosting services include GitHub, GitLab, and Bitbucket, among others. These platforms provide additional features like issue tracking, pull requests, and code review tools to enhance the collaborative development process.

To interact with remote repositories in Git, you use commands such as `git clone`, `git fetch`, `git pull`, and `git push`. These commands enable you to establish connections to remote repositories, synchronize changes between local and remote repositories, and collaborate with other developers effectively.

## 14. How do you push changes to a remote repository in Git?

To push changes from your local repository to a remote repository in Git, you can use the `git push` command. The `git push` command transfers your local commits to the corresponding branch in the remote repository, making your changes available to others. Here's how you can push changes:

1. Ensure that you have a remote repository set up and you have the necessary permissions to push changes to it. You can verify the configured remotes using the following command:

    * `git remote -v`

2. Commit your changes locally using `git commit` to create one or more commits. Make sure you have staged the changes using `git add` for the files you want to include in the commit.

3. Execute the following command to push the commits to the remote repository:

    * `git push <remote-name> <branch-name>`

    * Replace `<remote-name>` with the name of the remote repository, such as `origin`. Replace `<branch-name>` with the name of the branch to which you want to push the changes.

    * For example, to push changes to the `main` branch of the `origin` remote repository, you would use:

        * `git push origin main`

4. Git will attempt to push the commits to the remote repository. If you are pushing to the branch for the first time, Git may ask you to specify the upstream branch using the `--set-upstream` or `-u` option. This establishes a tracking relationship between the local branch and the remote branch.

5. If you are prompted to enter your credentials, provide your Git hosting service credentials (such as username and password, or an access token) to authenticate and authorize the push operation.

6. Once the push is successful, Git will display information about the pushed commits and any updated branch references in the remote repository.

By pushing your changes to a remote repository, you make them available to others working on the project. Other team members can then fetch or pull the changes from the remote repository to update their local copies.

It's worth noting that if multiple developers are working on the same branch and have pushed their changes, conflicts may arise. In such cases, Git provides mechanisms for conflict resolution, such as merge or rebase operations, to combine and reconcile conflicting changes.

Remember to always pull or fetch changes from the remote repository before pushing to avoid overwriting or losing others' work.

(So, when you execute `git push origin foo`, Git will push the commits and changes made in the local branch `foo` to the `foo` branch in the remote repository named `origin`. If the branch `foo` doesn't exist in the remote repository, Git will create it. If the branch already exists, Git will update it with the new commits and changes.)

## 15. What is the purpose of the "git fetch" command?

The `git fetch` command in Git is used to retrieve the latest changes from a remote repository without merging them into your local branch. It updates your local copy of the remote branches, allowing you to see and review the changes made by others. The purpose of `git fetch` is to synchronize your local repository with the remote repository, keeping you informed about the latest developments.

Here's how the `git fetch` command works:

1. Execute the following command to fetch the latest changes from the remote repository:

    * `git fetch <remote-name>`

    * Replace `<remote-name>` with the name of the remote repository, such as `origin`.

2. Git will connect to the specified remote repository and retrieve the latest commits and branch updates. It downloads the changes and updates the remote-tracking branches in your local repository, without modifying your working directory or current branch.

3. After executing `git fetch`, you can use commands like `git branch -r` or `git log --oneline --decorate --all` to see the updated remote branches and their commit history.

The git fetch command is useful in several scenarios:

* **Reviewing changes**: It allows you to see what others have committed to the remote repository before incorporating those changes into your local branch. You can inspect the fetched commits, review code changes, and discuss them with team members.

* **Updating your knowledge**: `git fetch` helps you stay up to date with the latest developments in the project. By fetching regularly, you can monitor the progress made by others, identify new features, bug fixes, or improvements, and make informed decisions for your own work.

* **Preparing for merge or rebase**: Fetching is often a prerequisite for merging or rebasing your local branch with the updated remote branch. By fetching first, you can ensure that your local copy is synchronized with the remote repository, minimizing conflicts and simplifying the merging or rebasing process.

It's important to note that `git fetch` does not automatically merge the fetched changes into your local branch. To incorporate the fetched changes, you can use `git merge`, `git rebase`, or other related commands, depending on your workflow and the desired outcome.

By regularly fetching changes from the remote repository, you can maintain an accurate and up-to-date view of the project's progress, collaborate effectively with others, and reduce the chances of code conflicts.

## 16. How do you undo the most recent commit in Git?

To undo the most recent commit in Git, you can use the `git reset` command. The `git reset` command allows you to move the branch pointer to a previous commit, effectively undoing the most recent commit. Here's how you can do it:

1. Execute the following command to undo the most recent commit:

    * `git reset HEAD~1`

    * This command moves the branch pointer one commit before the current commit (`HEAD~1` refers to the commit before `HEAD`).

    * Alternatively, if you want to preserve the changes from the undone commit as uncommitted changes in your working directory, you can use the following command:

        * `git reset HEAD~1 --soft`

2. After executing the command, Git will move the branch pointer to the previous commit, effectively undoing the most recent commit. The changes from the undone commit will be reflected as uncommitted changes in your working directory.

It's important to note that the `git reset` command modifies the commit history of your branch. If the commit you are undoing has already been pushed to a remote repository, you should avoid using `git reset` and consider using other methods, such as git revert, to undo the commit in a way that preserves the commit history and facilitates collaboration with others.

Additionally, if you have made changes in your working directory that you want to keep, you should either stash them or commit them before executing the `git reset` command. Otherwise, those changes might be lost.

Remember that once you have undone a commit, it is important to be cautious while pushing the changes to a remote repository. If others have already pulled the undone commit, it can cause conflicts or unexpected behavior.

If you want to completely discard the most recent commit, including any changes made in it, you can use the `--hard` option with `git reset`:

* `git reset HEAD~1 --hard`

This will move the branch pointer to the previous commit and discard any changes made in the undone commit. Use this option with caution, as it permanently discards changes without the ability to recover them.

## 17. What is a tag in Git and how do you create one?

[Tags](https://www.youtube.com/watch?v=spkUevg1NqM)

In Git, a tag marks an important point in a repository’s history. It's a way to label a specific commit with a meaningful and descriptive name. Tags are often used to mark important milestones, releases, or significant points in the commit history of a repository. Unlike branches, which can move and change as new commits are added, tags are immutable and remain fixed to the commit they are associated with. Git supports two types of tags:

* **Lightweight tags** point to specific commits, and contain no other information. Also known as soft tags. Create or remove them as needed.

  * `git tag <tag-name> <commit-hash>`

* **Annotated tags** contain metadata, can be signed for verification purposes, and can’t be changed. Annotated tags include additional information such as the tagger's name, email, and the date the tag was created.

  * `git tag -a <tag-name> <commit-hash> -m "Tag message"`

Common commands:

* View the list of tags in your repository:

  * `git tag`

* View the list of tags in repository and any messages:

  * `git tag -n`

* Delete a tag:

  * `git tag --delete (-d) <tag-name>`

By default, tags are local and are not automatically pushed to remote repositories. If you want to share your tags with others or make them available in a remote repository, you need to explicitly push the tags using the `git push` command with the `--tags` option:

* `git push origin --tags`

This command pushes all the tags in your local repository to the remote repository.

## 18. What is an upstream repository in Git and how do you set it up?

[Upstream](https://devopscube.com/set-git-upstream-respository-branch/)

Upstream refers to the original repo or a branch. For example, when you clone from Github, the remote Github repo is upstream for the cloned local copy.

types of git upstreams:

* **Git Repository Upstream**:

  * Whenever you clone a git repository, you get a local copy in your system. So, for your local copy, the actual repository is upstream.

* **Git Forked repository Upstream**:

  * When you clone a Forked repository to your local, the forked repository is considered as the remote origin, and the repository you forked from is upstream.

  * This workflow is common in open-source development and even in normal project developments.

  * For example, If you wanted to contribute to an open-source project, this is what you do:

    1. You would fork the actual project repository.

    2. Then you clone from the forked repository.

    3. Meanwhile, to keep up with the main open-source repository, you pull the changes from it through your git upstream config.

    4. You push your changes to a branch in the forked repository.

    5. Then you would raise a PR to the actual project repository from your forked repository.

## 19. What is the purpose of the "git bisect" command?

The `git bisect` command in Git is a powerful tool used for binary search-like debugging. Its purpose is to help identify the commit that introduced a bug or a regression by systematically narrowing down the range of commits to be investigated.

Here's how the `git bisect` command works:

1. Start by identifying a "good" and a "bad" commit:

    * A "good" commit is a known commit in your project's history where the bug or issue does not exist.

    * A "bad" commit is a known commit where the bug or issue is present.

2. Execute the following command to start the bisect process:

    * `git bisect start`

3. Specify the "good" and "bad" commits using their commit identifiers (e.g., commit hashes, branch names, or tags). For example:

    * `git bisect good <good-commit>`
    * `git bisect bad <bad-commit>`

4. Git will automatically checkout a commit in the middle of the identified range and prompt you to test if the bug is present or not.

5. Based on the test result, you mark the commit as "good" or "bad":

    * If the bug is present, use `git bisect bad`.

    * If the bug is not present, use `git bisect good`.

6. Repeat the process of testing and marking commits until Git identifies the specific commit that introduced the bug. Git will continue narrowing down the range until it isolates the faulty commit.

7. Once Git has identified the faulty commit, it will provide you with the commit information and end the bisect process.

The `git bisect` command automates the process of identifying the specific commit responsible for introducing a bug or regression. By systematically narrowing down the range of commits using binary search-like techniques, it significantly reduces the manual effort required for debugging.

The `git bisect` command can be a valuable tool when working with larger codebases or complex projects where identifying the root cause of a bug can be challenging. It helps in isolating the problematic commit, enabling further investigation, analysis, and potential fixes to address the issue.

After the bisect process is complete, you can use the information obtained to analyze the faulty commit, track down the changes that introduced the bug, and take appropriate actions such as fixing the code, reverting the commit, or reporting the issue.

Remember to end the bisect process using `git bisect reset` once you have completed the investigation and addressed the issue, or if you want to abort the bisect process before completion.

## 20. What are protected branches?

[Protected branches](https://www.youtube.com/watch?v=Q9WFrtJMnDI)

n GitLab, permissions are fundamentally defined around the idea of having read or write permission to the repository and branches. To impose further restrictions on certain branches, they can be protected.

A protected branch controls:

* Which users can merge into the branch.

* Which users can push to the branch.

* If users can force push to the branch.

* If changes to files listed in the CODEOWNERS file can be pushed directly to the branch.

* Which users can unprotect the branch.

The default branch for your repository is protected by default.

Protected branches provide an extra layer of control and security for critical branches in a repository. They help maintain code integrity, prevent unauthorized modifications, and enforce quality control measures. By using protected branches, teams can establish a more structured and controlled development process, particularly in projects with multiple contributors or where code stability is crucial.
